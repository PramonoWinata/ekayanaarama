package id.or.ekayana.ekayanaarama.activity.page;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;
import id.or.ekayana.ekayanaarama.app.file.EventImageFile;
import id.or.ekayana.ekayanaarama.data.db.EventDA;
import id.or.ekayana.ekayanaarama.model.EventImageItem;
import id.or.ekayana.ekayanaarama.model.EventItem;
import id.or.ekayana.ekayanaarama.service.DownloadEventImageService;
import id.or.ekayana.ekayanaarama.util.DateUtils;
import id.or.ekayana.ekayanaarama.util.ListUtils;

/**
 *
 * @author Jony
 * @since 02/07/2016
 */

//TODO check for image
public class EventDetailActivity extends BasePageActivity {

    /* ----------------------------------------- STATIC ----------------------------------------- */

    /**
     * Helps with conversion of {@code EventItem} to/from {@code Bundle}.
     */
    private static class EventItemBundle {
        private static final String EVENT_ID = "event_id",
                TITLE = "title",
                START_DATE = "start_date",
                END_DATE = "end_date",
                DESCRIPTION = "description",
                IMAGE = "thumbnail_uri";

        /**
         * Create {@code Bundle} from an event item.
         * @param e
         * @return
         */
        public static Bundle createBundle(EventItem e) {
            Bundle b = new Bundle(6);
            b.putLong(EVENT_ID, e.getID());
            b.putString(TITLE, e.getTitle());
            b.putString(START_DATE, e.getStartDateSQL());
            b.putString(END_DATE, e.getEndDateSQL());
            b.putString(DESCRIPTION, e.getDescription());
            b.putString(IMAGE, e.getThumbnailURI());
            return b;
        }

        /**
         * Parse a {@code Bundle} to an event item.
         * @param b
         * @return
         */
        public static EventItem parseBundle(Bundle b) {
            EventItem e = new EventItem(b.getLong(EVENT_ID));
            e.setTitle(b.getString(TITLE));
            e.setStartDateSQL(b.getString(START_DATE));
            e.setEndDateSQL(b.getString(END_DATE));
            e.setDescription(b.getString(DESCRIPTION));
            e.setThumbnailURI(b.getString(IMAGE));
            return e;
        }
    }

    // Extra names for intent calling this activity.
    private static final String EXTRA_EVENT_ITEM = EventDetailActivity.class.getCanonicalName() + ".EVENT_ITEM";

    public static void startActivity(Context context, EventItem eventItem) {
        Intent intent = new Intent(context, EventDetailActivity.class);
        intent.putExtra(EXTRA_EVENT_ITEM, EventItemBundle.createBundle(eventItem));
        context.startActivity(intent);
    }

    /* ----------------------------------------- MEMBERS ---------------------------------------- */

    private EventDA mEventDA = null;

    public EventDA getEventDA() {
        if (mEventDA == null) {
            mEventDA = new EventDA(this);
        }
        return mEventDA;
    }

    private List<Integer> mListMissingImageOrderNumber = new ArrayList<>(0);

    private void setMissingImageList(List<Integer> list) {
        synchronized (mListMissingImageOrderNumber) {
            // Set the missing image list.
            this.mListMissingImageOrderNumber.clear();
            this.mListMissingImageOrderNumber.addAll(list);
            // Start downloading missing images.
            if (!this.mListMissingImageOrderNumber.isEmpty()) {
                int[] orderNumbers = ListUtils.toArray(list);
                DownloadEventImageService.start(this, mEventItem.getID(), orderNumbers);
            }
        }
    }

    private boolean inMissingImageList(int orderNumber) {
        synchronized (mListMissingImageOrderNumber) {
            for (int i = 0, n = mListMissingImageOrderNumber.size(); i < n; i++) {
                if (orderNumber == mListMissingImageOrderNumber.get(i)) {
                    return true;
                }
            }
            return false;
        }
    }

    private void removeFromMissingImageList(int orderNumber) {
        synchronized (mListMissingImageOrderNumber) {
            if (!mListMissingImageOrderNumber.isEmpty()) {
                // Remove the order number from missing image list.
                for (int i = 0, n = mListMissingImageOrderNumber.size(); i < n; i++) {
                    if (orderNumber == mListMissingImageOrderNumber.get(i)) {
                        mListMissingImageOrderNumber.remove(i);
                        break;
                    }
                }
                // All missing images downloaded?
                if (mListMissingImageOrderNumber.isEmpty()) {
                }
            }
        }
    }

    private EventItem mEventItem = null;
    private List<EventImageItem> mListImageItem = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_event_detail);

        // Get intent data.
        mEventItem = EventItemBundle.parseBundle(getIntent().getBundleExtra(EXTRA_EVENT_ITEM));

        // Load the event detail.
        loadEventDetail();
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerLocalReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterLocalReceiver();
    }

    private void loadEventDetail() {
        // Set the title.
        setTitle(mEventItem.getTitle());

        // Set the event date.
        String sqlStartDate = mEventItem.getStartDateSQL(),
               sqlEndDate   = mEventItem.getEndDateSQL();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        String sDate = (sqlStartDate.equals(sqlEndDate))
                ? dateFormatter.format(DateUtils.parseSqlDate(sqlStartDate))
                : dateFormatter.format(DateUtils.parseSqlDate(sqlStartDate))
                    + " - " + dateFormatter.format(DateUtils.parseSqlDate(sqlEndDate));
        TextView txtDate = (TextView) findViewById(R.id.date);
        txtDate.setText(sDate);

        // Set the description.
        TextView txtDescription = (TextView) findViewById(R.id.description);
        if (mEventItem.hasDescription()) {
            txtDescription.setText(mEventItem.getDescription());
        } else {
            txtDescription.setVisibility(View.GONE);
        }

        // Load event images.
        //loadEventImages();

        ImageView imageView = (ImageView) findViewById(R.id.image);
        Picasso.with(this).load(mEventItem.getThumbnailURI()).into(imageView);
    }


    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Check broadcasted event ID.
            long eventID = intent.getLongExtra(DownloadEventImageService.EXTRA_EVENT_ID, 0);
            if (eventID != mEventItem.getID()) {
                return;
            }
            // Check intent action.
            //if (DownloadEventImageService.ACTION_REPORT_STATUS.equals(intent.getAction())) {
            //    onEventImageDownloaded(intent);
            //}
        }
    };
    private boolean mLocalReceiverRegistered = false;

    private void registerLocalReceiver() {
        if (!mLocalReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter(DownloadEventImageService.ACTION_REPORT_STATUS);
            LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, intentFilter);
            mLocalReceiverRegistered = true;
            System.out.println("Registered local broadcast receiver.");
        }
    }

    private void unregisterLocalReceiver() {
        if (mLocalReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
            mLocalReceiverRegistered = false;
            System.out.println("Unregistered local broadcast receiver.");
        }
    }
}


//Deprecated functions, replaced with picasso
/*    private void loadEventImages() {
        // Get event image list.
        mListImageItem = new EventDA(this).getEventImageList(mEventItem.getID());
        mEventItem.setImages(mListImageItem);

        // Get image container layout.
        //LinearLayout imageContainer = (LinearLayout) findViewById(R.id.image);
        LayoutParams imageViewLayoutParams = getImageViewLayoutParams();

        List<Integer> listMissingImage = new ArrayList<>(mListImageItem.size());

        // Remove all child views from the image container.
        //imageContainer.removeAllViews();
        //imageContainer.invalidate();
        for (EventImageItem imageItem : mListImageItem) {
            // Create {@code ImageView} for each image item, and add to the container.
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(imageViewLayoutParams);
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setTag(imageItem.getOrderNumber());
            //imageContainer.addView(imageView);

            // Set the image.
            if (!setImageView(imageView, imageItem.getSavedAs())) {
                // The event image has not been downloaded.
                // Add to list of missing images.
                listMissingImage.add(imageItem.getOrderNumber());
            }
        }

        // Download missing images.
        setMissingImageList(listMissingImage);
    }

    private LayoutParams getImageViewLayoutParams() {
        int marginPx = getResources().getDimensionPixelSize(R.dimen.text_margin);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.setMargins(0, marginPx, 0, 0);
        return params;
    }

    private boolean setImageView(ImageView imageView, String fileName) {
        // Check if the image file is saved locally.
        if (fileName != null && !fileName.isEmpty()) {
            EventImageFile file = new EventImageFile(this, fileName);
            if (file.exists()) {
                // Found.
                Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
                imageView.setImageBitmap(bitmap);
                // ImageView is set successfully.
                return true;
            }
        }
        // ImageView not set.
        return false;
    }

    private void onEventImageDownloaded(Intent intent) {
        boolean success = intent.getBooleanExtra(DownloadEventImageService.EXTRA_SUCCESS, false);
        if (success) {
            int[] orderNumbers = intent.getIntArrayExtra(DownloadEventImageService.EXTRA_ORDER_NUMBERS);
            String[] fileNames = intent.getStringArrayExtra(DownloadEventImageService.EXTRA_FILE_NAMES);

            //LinearLayout imageContainer = (LinearLayout) findViewById(R.id.image);
            for (int i = 0; i < orderNumbers.length; i++) {
                try {
                    int orderNumber = orderNumbers[i];
                    if (!inMissingImageList(orderNumber)) {
                        loadEventImages();
                    }
                    String savedAs = fileNames[i];
                    if (savedAs != null) {
                        ImageView imageView = (ImageView) imageContainer.findViewWithTag(orderNumber);
                        if (imageView != null && setImageView(imageView, savedAs)) {
                            removeFromMissingImageList(orderNumber);
                        } else {
                            loadEventImages();
                        }
                    }
                } catch (RuntimeException ex) { }
            }
        } else {
            String message = intent.getStringExtra(DownloadEventImageService.EXTRA_MESSAGE);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }*/
