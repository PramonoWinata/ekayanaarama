package id.or.ekayana.ekayanaarama.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;

import java.util.Calendar;

import id.or.ekayana.ekayanaarama.activity.page.DailyReflectionActivity;
import id.or.ekayana.ekayanaarama.constant.NotificationID;
import id.or.ekayana.ekayanaarama.receiver.NotificationPublisher;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests
 * to display daily reflection notifications.
 *
 * @author Jony
 * @since  03/08/2017
 */
public class DailyReflectionService extends IntentService {
    private static final String ACTION_NOTIFY_DAILY_REFLECTION = "id.or.ekayana.ekayanaarama.service.action.NOTIFY_DAILY_REFLECTION";

    /**
     * When to notify (hour of day).
     */
    private static final int NOTIFICATION_HOUR = 7;

    /**
     * Starts the service in a repeating interval.
     * @param context
     */
    public static void startRepeating(Context context) {
        // Calculate first run trigger time.
        Calendar calendar = Calendar.getInstance();
        if (calendar.get(Calendar.HOUR_OF_DAY) >= NOTIFICATION_HOUR) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        calendar.set(Calendar.HOUR_OF_DAY, NOTIFICATION_HOUR);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        // Set the repeating alarm.
        Intent intent = new Intent(context, DailyReflectionService.class);
        intent.setAction(ACTION_NOTIFY_DAILY_REFLECTION);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        PendingIntent alarmIntent = PendingIntent.getService(context, 0, intent, 0);
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
    }

    public DailyReflectionService() {
        super("DailyReflectionService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String intentAction = intent.getAction();
            if (ACTION_NOTIFY_DAILY_REFLECTION.equals(intentAction)) {
                handleActionNotifyDailyReflection();
            }
        }
    }

    private void handleActionNotifyDailyReflection() {
        NotificationPublisher.publish(this, NotificationID.DAILY_REFLECTION, "Renungan Harian", "Baca renungan hari ini.", DailyReflectionActivity.class);
    }
}
