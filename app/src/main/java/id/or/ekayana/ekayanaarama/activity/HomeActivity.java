package id.or.ekayana.ekayanaarama.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BaseActivity;
import id.or.ekayana.ekayanaarama.activity.page.AboutEkayanaActivity;
import id.or.ekayana.ekayanaarama.activity.page.AppInfoActivity;
import id.or.ekayana.ekayanaarama.activity.page.BuddhavacanaActivity;
import id.or.ekayana.ekayanaarama.activity.page.ContactActivity;
import id.or.ekayana.ekayanaarama.activity.page.DailyReflectionActivity;
import id.or.ekayana.ekayanaarama.activity.page.EkayanaActivitiesActivity;
import id.or.ekayana.ekayanaarama.activity.page.EventsActivity;
import id.or.ekayana.ekayanaarama.activity.page.MoreMenuActivity;
import id.or.ekayana.ekayanaarama.activity.page.PreferencesActivity;
import id.or.ekayana.ekayanaarama.app.adapter.MainMenuAdapter;
import id.or.ekayana.ekayanaarama.constant.ShPrefsName;
import id.or.ekayana.ekayanaarama.model.MenuItem;

/**
 * Home screen, contains main menu.
 *
 * @author Jony
 * @since 01/07/2016
 */
public class HomeActivity extends BaseActivity {

    private static abstract class MainMenu {
        private static final int MENU_DAILY_REFLECTION = 1,
                                 MENU_BUDDHAVACANA     = 2,
                                 MENU_ABOUT_EKAYANA    = 3,
                                 MENU_ACTIVITIES       = 4,
                                 MENU_EVENTS           = 5,
                                 MENU_CONTACT          = 6,
                                 MENU_PREFERENCES      = 7,
                                 MENU_ABOUT_APP        = 8;

        public static ArrayList<MenuItem> getItems(Context ctx) {
            ArrayList<MenuItem> list = new ArrayList<>(7);
            list.add(new MenuItem(MENU_DAILY_REFLECTION, ctx.getString(R.string.activity_daily_reflection), R.drawable.ic_menu_daily, DailyReflectionActivity.class));
            list.add(new MenuItem(MENU_BUDDHAVACANA,     ctx.getString(R.string.activity_buddhavacana), R.drawable.ic_menu_buddhavacana, BuddhavacanaActivity.class));
            list.add(new MenuItem(MENU_ABOUT_EKAYANA,    ctx.getString(R.string.activity_about_ekayana), R.drawable.ic_menu_about_ekayana, AboutEkayanaActivity.class));
            list.add(new MenuItem(MENU_ACTIVITIES,       ctx.getString(R.string.activity_activities), R.drawable.ic_menu_activities, EkayanaActivitiesActivity.class));
            list.add(new MenuItem(MENU_EVENTS,           ctx.getString(R.string.activity_events), R.drawable.ic_menu_events, EventsActivity.class));
            list.add(new MenuItem(MENU_CONTACT,          ctx.getString(R.string.activity_contact), R.drawable.ic_menu_contact, ContactActivity.class));
            // list.add(new MenuItem(MENU_PREFERENCES,      ctx.getString(R.string.activity_preferences), R.drawable.ic_menu_preferences, PreferencesActivity.class)); // TODO: Temporary!
            list.add(new MenuItem(MENU_ABOUT_APP,        ctx.getString(R.string.activity_about_app), R.drawable.ic_menu_about_app, AppInfoActivity.class));
            return list;
        }
    }

    private static final String DATE_FORMAT_SH = "yyyy-MM-dd";

    private GridView mMenuGridView = null;

    private void setMainMenu() {
        // Set the main menu items.
        ArrayList<MenuItem> listMenuItem = MainMenu.getItems(this);
        MainMenuAdapter adapter = new MainMenuAdapter(this, listMenuItem);
        mMenuGridView.setAdapter(adapter);

        // Set the click listener.
        mMenuGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    MenuItem menuItem = (MenuItem) view.getTag(R.id.menu_item);
                    if (menuItem.isEnabled()) {
                        Intent intent = new Intent(HomeActivity.this, menuItem.getActivityClass());
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in, android.R.anim.fade_out);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_home);
        mMenuGridView = (GridView) findViewById(R.id.menu_list);

        // Load main menu.
        setMainMenu();

        // Check if it is the first time the app runs today.
        SharedPreferences shPref = PreferenceManager.getDefaultSharedPreferences(this);
        String lastLaunchDate = shPref.getString(ShPrefsName.Key.LAST_RUN_DATE, null),
               today = new SimpleDateFormat(DATE_FORMAT_SH).format(new Date());
        if (!today.equals(lastLaunchDate)) {
            // The first run for today.
            // Show activity `Daily Reflection`.
            Intent intent = new Intent(this, DailyReflectionActivity.class);
            startActivity(intent);
            // Update the last running date.
            shPref.edit().putString(ShPrefsName.Key.LAST_RUN_DATE, today).apply();
        }
    }
}
