package id.or.ekayana.ekayanaarama.constant;

/**
 * Notification IDs.
 *
 * @author Jony
 * @since 03/08/2017
 */

public abstract class NotificationID {
    public static final int
            DAILY_REFLECTION = 1,
            EVENT_UPDATE = 2;
}
