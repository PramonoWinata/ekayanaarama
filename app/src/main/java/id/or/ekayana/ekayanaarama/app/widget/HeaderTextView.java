package id.or.ekayana.ekayanaarama.app.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

import id.or.ekayana.ekayanaarama.R;

/**
 * @author Jony
 * @since 06/11/2016
 */

public class HeaderTextView extends TextView {
    public HeaderTextView(Context context) {
        super(context);
        init();
    }

    public HeaderTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HeaderTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        float textSize = getResources().getDimension(R.dimen.text_size_header);
        setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        setTypeface(getTypeface(), Typeface.BOLD);
        setGravity(Gravity.CENTER);
        if (Build.VERSION.SDK_INT >= 17) {
            setTextAlignment(TEXT_ALIGNMENT_CENTER);
        }
    }
}
