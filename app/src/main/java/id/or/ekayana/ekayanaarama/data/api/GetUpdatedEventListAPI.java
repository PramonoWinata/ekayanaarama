package id.or.ekayana.ekayanaarama.data.api;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import id.or.ekayana.ekayanaarama.model.EventImageItem;
import id.or.ekayana.ekayanaarama.model.EventItem;

/**
 * Handles data requests to server.
 *
 * @author Jony
 * @since 13/11/2016
 */

public class GetUpdatedEventListAPI extends BaseAPI {
    private static final String URI = "api/get_event";

    /**
     * The API response.
     */
    public static class Response {
        private final List<EventItem> listEvent;
        private final long timestamp;

        public Response(List<EventItem> listEvent, long timestamp) {
            this.listEvent = listEvent;
            this.timestamp = timestamp;
        }

        /**
         * Get the list of updated events returned by the API.
         * @return
         */
        public List<EventItem> getEventList() { return listEvent; }

        /**
         * If there are any updated events.
         * @return
         */
        public boolean hasUpdatedEvents() {
            return !listEvent.isEmpty();
        }

        /**
         * Get the timestamp of the response.
         * @return
         */
        public long getTimestamp() {
            return timestamp;
        }
    }

    /**
     * Constructs.
     * @param context
     */
    public GetUpdatedEventListAPI(Context context) {
        super(context);
    }

    /**
     * Execute request to the API.
     * @param lastTimestamp Timestamp of the previous request
     * @return The response
     * @throws IOException if an error occurs while sending the request
     * @throws RuntimeException if an error occurs while parsing the response
     */
    public Response execute(long lastTimestamp) throws IOException, RuntimeException {
        Map<String, String> parameters = new LinkedHashMap<>(1);


        //1604188800
        //parameters.put("timestamp", String.valueOf(lastTimestamp));

        //hard code to get events
        parameters.put("timestamp", "1514764800");

        String response = sendRequest(URI, parameters);

        return parseResponse(response);
    }

    private Response parseResponse(String response) {

        try {
            JSONObject json = new JSONObject(response);
            JSONArray items = json.getJSONArray("items");
            long timestamp = json.getLong("timestamp");

            int totalItems = items.length();
            ArrayList<EventItem> listEvent = new ArrayList<>(totalItems);
            for (int i = 0; i < totalItems; i++) {
                JSONObject item = items.getJSONObject(i);
                long eventID = item.getLong("id");
                EventItem e = new EventItem(eventID, (item.getInt("isActiveRecord") == 1));
                if (e.isActiveRecord()) {
                    // Set the event detail.
                    e.setTitle(item.getString("title"));
                    e.setDescription(item.getString("description"));
                    e.setPublishDateSQL(item.getString("publishDate"));
                    e.setStartDateSQL(item.getString("startDate"));
                    e.setEndDateSQL(item.getString("endDate"));
                    e.setLastModified(item.getLong("lastModified"));

                    //TODO SEE IMAGE HERE
                    // Set the event images.
                    JSONArray arrImageURIs = item.getJSONArray("imageURIs");
                    int countImage = arrImageURIs.length();
                    List<EventImageItem> listImage = new ArrayList<>(countImage);
                    for (int x = 0; x < countImage;) {
                        String originalURI = arrImageURIs.getString(x);

                        //set thumbnail
                        if(x == 0)
                            e.setThumbnailURI(originalURI);

                        x++;
                        EventImageItem img = new EventImageItem(eventID, x);
                        img.setOriginalURI(originalURI);
                        listImage.add(img);
                    }
                    e.setImages(listImage);
                }
                listEvent.add(e);
            }

            return new Response(listEvent, timestamp);
        } catch (JSONException ex) {
            throw new RuntimeException("parseResponse()", ex);
        }
    }
}
