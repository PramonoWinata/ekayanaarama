package id.or.ekayana.ekayanaarama.data.download;

import android.content.Context;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import id.or.ekayana.ekayanaarama.app.exception.NetworkNotConnectedException;
import id.or.ekayana.ekayanaarama.util.ConnectionUtils;

/**
 * Handles downloads from internet.
 *
 * @author Jony
 * @since 27/12/2016
 */

public class Downloader {
    private static final int CONNECTION_TIMEOUT_MILLIS = 10 * 1000;

    private final Context mContext;
    private final String mSaveDirPath;

    public Downloader(Context context, File saveDir) {
        this(context, (saveDir !=  null)
                ? saveDir.getPath()
                : context.getFilesDir().getPath());
    }

    public Downloader(Context context, String saveDirPath) {
        if (saveDirPath == null || saveDirPath.isEmpty()) {
            saveDirPath = context.getFilesDir().getPath();
        }
        this.mContext = context;
        this.mSaveDirPath = saveDirPath;
    }

    public Context getContext() {
        return mContext;
    }

    /**
     * Download from <code>url</code> and save to specified directory.
     *
     * @param url The URL to download
     * @param saveFileName The file name the download file is to be saved as
     * @return The saved file name, if the download is successful
     * @throws IOException
     */
    public String download(String url, String saveFileName) throws IOException {
        // Check network connection.
        if (!ConnectionUtils.isNetworkAvailable(mContext)) {
            throw new NetworkNotConnectedException("Not connected to network.");
        }

        // Open the connection.
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setConnectTimeout(CONNECTION_TIMEOUT_MILLIS);

        // Create instance for the download file.
        if (saveFileName == null ||saveFileName.isEmpty()) {
            int r = (int) Math.random() * 899 + 100;
            saveFileName = String.valueOf(System.currentTimeMillis()) + String.valueOf(r);
        }
        File file = new File(mSaveDirPath, saveFileName);

        // Get the input stream and write to file.
        InputStream inputStream = connection.getInputStream();
        OutputStream outputStream = new FileOutputStream(file);
        try {
            byte[] buffer = new byte[4096];
            int count;
            while ((count = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, count);
            }
            outputStream.flush();
        } catch (IOException ex) {
            throw ex;
        } finally {
            try {
                outputStream.close();
            } catch (IOException ex) { }
        }

        // Return the saved file name.
        return saveFileName;
    }
}
