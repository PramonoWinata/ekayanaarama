package id.or.ekayana.ekayanaarama.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.or.ekayana.ekayanaarama.R;

/**
 * Header tab for "About Ekayana".
 *
 * @author Jony
 * @since 18/09/2016
 */
public class AboutEkayanaHeaderFragment extends Fragment {

    public AboutEkayanaHeaderFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about_ekayana_header, container, false);
    }

}
