package id.or.ekayana.ekayanaarama.activity.page;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;
import id.or.ekayana.ekayanaarama.app.adapter.DailyActivityAdapter;
import id.or.ekayana.ekayanaarama.constant.Strings;
import id.or.ekayana.ekayanaarama.model.ActivityItem;
import id.or.ekayana.ekayanaarama.model.ActivityScheduleItem;
import id.or.ekayana.ekayanaarama.util.FileUtils;
import id.or.ekayana.ekayanaarama.util.StringUtils;

/**
 * Shows Ekayana's routine activities.
 *
 * @author Jony
 * @since 11/07/2016
 */


//TODO make it into adapter list

public class EkayanaActivitiesActivity extends BasePageActivity {

    private static class EkayanaActivities {
        private static final String ASSET_FILE_NAME        = "activities";
        private static final String ITEM_ACTIVITIES        = "activities",
                                    ITEM_INFO              = "info",
                                    ITEM_NAME              = "name",
                                    ITEM_SCHEDULES         = "schedules",
                                    ITEM_SCHEDULE_DAY      = "day",
                                    ITEM_SCHEDULE_TIME     = "time",
                                    ITEM_SCHEDULE_LOCATION = "location";



        /**
         * Get routine activities.
         *
         * @param context The context
         * @return The list of routine activities
         */
        public static List<ActivityItem> get(Context context) {
            try {
                // Get the file content from assets.
                String fileContent = FileUtils.readAssetFile(context, ASSET_FILE_NAME);

                // Parse the file content to JSON, and get the item list.
                JSONArray root = new JSONObject(fileContent).getJSONArray(ITEM_ACTIVITIES);
                List<ActivityItem> listActivity = new ArrayList<>(root.length());

                // Get the item data.
                for (int i = 0, n = root.length(); i < n; i++) {
                    JSONObject jsonActivity = root.getJSONObject(i);
                    if (jsonActivity.has(ITEM_NAME)) {
                        JSONArray jsonSchedules = jsonActivity.getJSONArray(ITEM_SCHEDULES);
                        String activityName = jsonActivity.getString(ITEM_NAME);

                        List<ActivityScheduleItem> listSchedule = new ArrayList<>(jsonSchedules.length());
                        ActivityItem item = new ActivityItem(activityName, listSchedule);
                        for (int s = 0; s < jsonSchedules.length(); s++) {
                            JSONObject jsonSchedule = jsonSchedules.getJSONObject(s);
                            String  scheduleDay = jsonSchedule.getString(ITEM_SCHEDULE_DAY),
                                    scheduleTime = jsonSchedule.getString(ITEM_SCHEDULE_TIME),
                                    scheduleLocation = (!jsonSchedule.isNull(ITEM_SCHEDULE_LOCATION))
                                            ? jsonSchedule.getString(ITEM_SCHEDULE_LOCATION)
                                            : null;
                            listSchedule.add(new ActivityScheduleItem(scheduleDay, scheduleTime, scheduleLocation));
                        }

                        listActivity.add(item);
                    } else {
                        JSONArray jsonInfo = jsonActivity.getJSONArray(ITEM_INFO);
                        List<String> listInfo = new ArrayList<>(jsonInfo.length());
                        for (int s = 0; s < jsonInfo.length(); s++) {
                            listInfo.add(jsonInfo.getString(s));
                        }
                        listActivity.add(new ActivityItem(listInfo));
                    }
                }

                // Return the list.
                return listActivity;
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
                String msg = "No data found";
                throw new RuntimeException(msg, ex);
            } catch (JSONException ex) {
                ex.printStackTrace();
                String msg = "Error loading data";
                throw new RuntimeException(msg, ex);
            }
        }
    }

    private RecyclerView mListView;
    private DailyActivityAdapter dailyActivityAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_ekayana_activities);

        //List
        mListView = (RecyclerView) findViewById(R.id.list);

        // Get the data.
        List<ActivityItem> listActivity;


        try {
            listActivity = EkayanaActivities.get(this);
        } catch (RuntimeException ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }

        // Load the page.
        loadPage(listActivity);
    }

    private void loadPage(List<ActivityItem> listActivity) {

        //LinearLayout contentContainer = (LinearLayout) findViewById(R.id.content);

        dailyActivityAdapter = new DailyActivityAdapter(listActivity);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mListView.setLayoutManager(mLayoutManager);
        mListView.setAdapter(dailyActivityAdapter);

        // Calculate margins in px.
        float density = getResources().getDisplayMetrics().density;
        int marginTopPxActivity = getResources().getDimensionPixelSize(R.dimen.text_margin),
            marginTopPxSchedule = (int) Math.round(4 * density);

        // Set the layout params for each activity item.
        LinearLayout.LayoutParams layoutParamsActivity = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParamsActivity.setMargins(0, marginTopPxActivity, 0, 0);

        // Set the layout params for each schedule item.
        LinearLayout.LayoutParams layoutParamsSchedule = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParamsSchedule.setMargins(0, marginTopPxSchedule, 0, 0);

        for (int i = 0, n = listActivity.size(); i < n; i++) {
            ActivityItem activity = listActivity.get(i);

            if (activity.isInfo()) {
                TextView textInfo = new TextView(this);
                if (i == 0) {
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0, 0, 0, 0);
                    textInfo.setLayoutParams(layoutParams);
                } else {
                    textInfo.setLayoutParams(layoutParamsActivity);
                }
                List<String> listInfo = activity.getInfoList();
                String sInfo = Strings.EMPTY;
                for (int s = 0, size = listInfo.size(); s < size; s++) {
                    if (s == 0) {
                        sInfo = listInfo.get(s);
                    } else {
                        sInfo += "<br />" + listInfo.get(s);
                    }
                }
                textInfo.setText(Html.fromHtml(sInfo));
                //contentContainer.addView(textInfo);
            } else {
                // Create {@code TextView} for each activity item.
                TextView textActivityName = new TextView(this);
                if (i == 0) {
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0, 0, 0, 0);
                    textActivityName.setLayoutParams(layoutParams);
                } else {
                    textActivityName.setLayoutParams(layoutParamsActivity);
                }
                textActivityName.setText(Html.fromHtml("<strong>" + activity.getName() + "</strong>"));
                //contentContainer.addView(textActivityName);

                for (ActivityScheduleItem schedule : activity.getScheduleList()) {
                    String s = schedule.getDay();
                    if (schedule.getTime().startsWith("<br />")) {
                        s += schedule.getTime();
                    } else {
                        s += ", " + schedule.getTime();
                    }
                    if (schedule.getLocation() != null) {
                        s += "<br />" + schedule.getLocation();
                    }

                    TextView textScheduleItem = new TextView(this);
                    textScheduleItem.setLayoutParams(layoutParamsSchedule);
                    textScheduleItem.setText(Html.fromHtml(s));
                    //contentContainer.addView(textScheduleItem);
                }
            }
        }
    }
}
