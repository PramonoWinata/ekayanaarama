package id.or.ekayana.ekayanaarama.app;

import id.or.ekayana.ekayanaarama.BuildConfig;

/**
 * Application configurations.
 *
 * @author Jony
 * @since 29/12/2016
 */
public abstract class AppConfig {
    public static final String SERVER_HOST = BuildConfig.SERVER_HOST;
}
