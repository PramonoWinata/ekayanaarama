package id.or.ekayana.ekayanaarama.activity.page;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;
import id.or.ekayana.ekayanaarama.app.adapter.ViewPagerAdapter;
import id.or.ekayana.ekayanaarama.fragment.AboutEkayanaChapterFragment;
import id.or.ekayana.ekayanaarama.fragment.AboutEkayanaHeaderFragment;

/**
 *
 * @author Jony
 * @since 02/07/2016
 */
public class AboutEkayanaActivity extends BasePageActivity {

    private static LinkedHashMap<String, Integer> mapTabs = new LinkedHashMap<>(8);
    static {
        mapTabs.put("Sejarah Singkat", R.string.about_ekayana_history);
        mapTabs.put("Visi dan Misi", R.string.about_ekayana_vision_mission);
        mapTabs.put("Kebaktian dan Pelayanan Umat", R.string.about_ekayana_schedule_service);
        mapTabs.put("Pendidikan dan Pelatihan", R.string.about_ekayana_education);
        mapTabs.put("Pelayanan Sosial", R.string.about_ekayana_social);
        mapTabs.put("Media dan Unit Usaha", R.string.about_ekayana_media);
        mapTabs.put("Generasi Muda, Seni Budaya, dan Olahraga", R.string.about_ekayana_youth_culture_sport);
    }

    private ViewPager mViewPager;

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addItem("Wihara", new AboutEkayanaHeaderFragment());
        for (String title : mapTabs.keySet()) {
            adapter.addItem(title, AboutEkayanaChapterFragment.newInstance(mapTabs.get(title)));
        }
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_about_ekayana);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }
}