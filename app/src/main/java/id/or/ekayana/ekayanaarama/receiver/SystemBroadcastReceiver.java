package id.or.ekayana.ekayanaarama.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import id.or.ekayana.ekayanaarama.service.SynchronizeEventListService;

/**
 * Receives broadcast intents sent by system.
 *
 * @author Jony
 * @since 15/02/2017
 */

public class SystemBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && !action.isEmpty()) {
            switch (action) {
                case Intent.ACTION_BOOT_COMPLETED:
                    // Run scheduled service to sync the event list, with 1 minute delay to the first run.
                    SynchronizeEventListService.startRepeating(context, 60000);
                    break;
            }
        }
    }
}
