package id.or.ekayana.ekayanaarama.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Adapter for view pager.
 *
 * @author Jony
 * @since 18/09/2016
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final ArrayList<String> mTitles = new ArrayList<>(8);
    private final ArrayList<Fragment> mFragments = new ArrayList<>(8);

    /**
     * Constructor.
     * @param fm
     */
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    /**
     * Get number of tabs/pages.
     * @return
     */
    @Override
    public int getCount() {
        return mFragments.size();
    }

    /**
     * Get fragment at the specified <code>position</code>.
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    /**
     * Get page title at the specified <code>position</code>.
     * @param position
     * @return
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }

    /**
     * Add new item/page.
     * @param title
     * @param page
     */
    public void addItem(String title, Fragment page) {
        mTitles.add(title);
        mFragments.add(page);
    }
}