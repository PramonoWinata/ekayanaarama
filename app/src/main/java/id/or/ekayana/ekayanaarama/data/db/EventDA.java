package id.or.ekayana.ekayanaarama.data.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.or.ekayana.ekayanaarama.data.db.contract.EventContract;
import id.or.ekayana.ekayanaarama.data.db.contract.EventImageContract;
import id.or.ekayana.ekayanaarama.model.EventImageItem;
import id.or.ekayana.ekayanaarama.model.EventItem;
import id.or.ekayana.ekayanaarama.util.ListUtils;

/**
 * Handles event data for local database.
 *
 * @author Jony
 * @since 17/12/2016
 */

public class EventDA {

    private final DatabaseHelper mDatabaseHelper;

    public EventDA(Context context) {
        this.mDatabaseHelper = new DatabaseHelper(context);
    }

    /**
     * Get timestamp of the last modified event item.
     *
     * @return The timestamp
     */
    public long getLatestRecordTimestamp() {
        try {
            // Get the database.
            SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();

            // Query the database.
            final String sql = "SELECT MAX(" + EventContract.Columns.LAST_MODIFIED + ")"
                    + " FROM " + EventContract.TABLE_NAME;
            Cursor cursor = db.rawQuery(sql, null);

            // Retrieve the results.
            try {
                cursor.moveToFirst();
                return (cursor.isAfterLast())
                        ? 0
                        : cursor.getLong(0);
            } finally {
                cursor.close();
            }
        } finally {
            // Close the database.
            mDatabaseHelper.close();
        }
    }

    /**
     * Get the list of published events which have not ended.
     *
     * @return The event list
     */
    public List<EventItem> getEventList() {
        try {
            // Get date string of today.
            String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            //hard code to get the events
            today = "2017-08-08";

            // Get the database.
            SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();

            // Query the database.
            String[] selectColumns = {
                    EventContract.Columns.EVENT_ID,
                    EventContract.Columns.TITLE,
                    EventContract.Columns.START_DATE,
                    EventContract.Columns.END_DATE,
                    EventContract.Columns.DESCRIPTION,
                    EventContract.Columns.THUMBNAIL_URI
            };
            final String sql = "SELECT " + ListUtils.joinToString(selectColumns, ", ")
                    + " FROM " + EventContract.TABLE_NAME
                    + " WHERE " + EventContract.Columns.PUBLISH_DATE + " <= ?"
                        + " AND " + EventContract.Columns.END_DATE + " >= ?"
                    + " ORDER BY " + EventContract.Columns.START_DATE + " ASC";
            String[] args = { today, today };
            Cursor cursor = db.rawQuery(sql, args);

            // Get column indices.
            int colEventID = cursor.getColumnIndex(EventContract.Columns.EVENT_ID),
                colTitle = cursor.getColumnIndex(EventContract.Columns.TITLE),
                colStartDate = cursor.getColumnIndex(EventContract.Columns.START_DATE),
                colEndDate = cursor.getColumnIndex(EventContract.Columns.END_DATE),
                colDescription = cursor.getColumnIndex(EventContract.Columns.DESCRIPTION),
                colThumbnail = cursor.getColumnIndex(EventContract.Columns.THUMBNAIL_URI);

            // Retrieve the results.
            try {
                List<EventItem> listItem = new ArrayList<>(cursor.getCount());
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    EventItem item = new EventItem(cursor.getLong(colEventID));
                    item.setTitle(cursor.getString(colTitle));
                    item.setStartDateSQL(cursor.getString(colStartDate));
                    item.setEndDateSQL(cursor.getString(colEndDate));
                    item.setDescription(cursor.getString(colDescription));
                    item.setThumbnailURI(cursor.getString(colThumbnail));
                    listItem.add(item);
                    cursor.moveToNext();
                }

                return listItem;
            } finally {
                cursor.close();
            }
        } finally {
            // Close the database.
            mDatabaseHelper.close();
        }
    }

    /**
     * Get the list of all images of an event.
     *
     * @param  eventID The event ID
     * @return The event image list
     */
    public List<EventImageItem> getEventImageList(long eventID) {
        return getEventImageList(eventID, null);
    }

    /**
     * Get the list of images of an event.
     *
     * @param  eventID The event ID
     * @param  orderNumbers The order numbers of the event images
     * @return The event image list
     */
    public List<EventImageItem> getEventImageList(long eventID, int[] orderNumbers) {
        boolean hasOrderNum = (orderNumbers != null && orderNumbers.length > 0);
        try {
            // Get the database.
            SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();

            // Query the database.
            String[] selectColumns = {
                    EventImageContract.Columns.ORDER_NUMBER,
                    EventImageContract.Columns.ORIGINAL_URI,
                    EventImageContract.Columns.SAVED_AS,
                    EventImageContract.Columns.SAVED_TIME
            };
            String where = EventImageContract.Columns.EVENT_ID + " = ?";
            String[] args;
            if (hasOrderNum) {
                StringBuilder sbParams = new StringBuilder(orderNumbers.length * 3 - 2)
                        .append("?");
                for (int i = 1; i < orderNumbers.length; i++) {
                    sbParams.append(", ?");
                }
                where += " AND " + EventImageContract.Columns.ORDER_NUMBER + " IN (" + sbParams.toString() + ")";
                args = new String[orderNumbers.length + 1];
                args[0] = String.valueOf(eventID);
                int nextIndex = 1;
                for (int o : orderNumbers) {
                    args[nextIndex++] = String.valueOf(o);
                }
            } else {
                args = new String[] { String.valueOf(eventID) };
            }
            final String sql = "SELECT " + ListUtils.joinToString(selectColumns, ", ")
                    + " FROM " + EventImageContract.TABLE_NAME
                    + " WHERE " + where
                    + " ORDER BY " + EventImageContract.Columns.ORDER_NUMBER + " ASC";
            Cursor cursor = db.rawQuery(sql, args);

            // Get column indices.
            int colOrderNumber = cursor.getColumnIndex(EventImageContract.Columns.ORDER_NUMBER),
                colOriginalURI = cursor.getColumnIndex(EventImageContract.Columns.ORIGINAL_URI),
                colSavedAs = cursor.getColumnIndex(EventImageContract.Columns.SAVED_AS),
                colSavedTime = cursor.getColumnIndex(EventImageContract.Columns.SAVED_TIME);

            // Retrieve the results.
            try {
                List<EventImageItem> listItem = new ArrayList<>(cursor.getCount());
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    EventImageItem item = new EventImageItem(eventID, cursor.getInt(colOrderNumber));
                    item.setOriginalURI(cursor.getString(colOriginalURI));
                    item.setSavedAs(cursor.getString(colSavedAs));
                    item.setSavedTimestamp(cursor.getLong(colSavedTime));
                    listItem.add(item);
                    cursor.moveToNext();
                }
                return listItem;
            } finally {
                cursor.close();
            }
        } finally {
            // Close the database.
            mDatabaseHelper.close();
        }
    }

    /**
     * Get list of event images with locally saved file.
     *
     * @return The list of saved file names
     */
    public List<String> getSavedEventImageFileList() {
        try {
            // Get the database.
            SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();

            // Query the database.
            final String sql = "SELECT " + EventImageContract.Columns.SAVED_AS
                    + " FROM " + EventImageContract.TABLE_NAME
                    + " WHERE " + EventImageContract.Columns.SAVED_AS + " IS NOT NULL"
                    + " ORDER BY " + EventImageContract.Columns.SAVED_AS + " ASC";
            Cursor cursor = db.rawQuery(sql, null);

            // Get column indices.
            int colSavedAs = cursor.getColumnIndex(EventImageContract.Columns.SAVED_AS);

            // Retrieve the results.
            try {
                List<String> listItem = new ArrayList<>(cursor.getCount());
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    listItem.add(cursor.getString(colSavedAs));
                    cursor.moveToNext();
                }
                return listItem;
            } finally {
                cursor.close();
            }
        } finally {
            // Close the database.
            mDatabaseHelper.close();
        }
    }

    /**
     * Get list of event images whose files have not been locally saved yet.
     *
     * @return The list of event image items
     */
    public List<EventImageItem> getUnsavedEventImageList() {
        try {
            // Get date string of today.
            String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            // Get the database.
            SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();

            // Query the database.
            String[] selectColumns = {
                    "img." + EventImageContract.Columns.EVENT_ID,
                    "img." + EventImageContract.Columns.ORDER_NUMBER,
                    "img." + EventImageContract.Columns.ORIGINAL_URI
            };
            final String sql = "SELECT " + ListUtils.joinToString(selectColumns, ", ")
                    + " FROM " + EventImageContract.TABLE_NAME + " img"
                    + " JOIN " + EventContract.TABLE_NAME + " evt"
                        + " ON evt." + EventContract.Columns.EVENT_ID + " = img." + EventImageContract.Columns.EVENT_ID
                    + " WHERE img." + EventImageContract.Columns.SAVED_AS + " IS NULL"
                        + " AND evt." + EventContract.Columns.END_DATE + " >= ? "
                    + " ORDER BY " + EventContract.Columns.START_DATE + " DESC, "
                                   + EventImageContract.Columns.ORDER_NUMBER + " ASC";
            String[] args = { today };
            Cursor cursor = db.rawQuery(sql, args);

            // Get column indices.
            int colEventID     = cursor.getColumnIndex(EventImageContract.Columns.EVENT_ID),
                colOrderNumber = cursor.getColumnIndex(EventImageContract.Columns.ORDER_NUMBER),
                colOriginalURI = cursor.getColumnIndex(EventImageContract.Columns.ORIGINAL_URI);

            // Retrieve the results.
            try {
                List<EventImageItem> listItem = new ArrayList<>(cursor.getCount());
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    EventImageItem item = new EventImageItem(cursor.getLong(colEventID), cursor.getInt(colOrderNumber));
                    item.setOriginalURI(cursor.getString(colOriginalURI));
                    listItem.add(item);
                    cursor.moveToNext();
                }
                return listItem;
            } finally {
                cursor.close();
            }
        } finally {
            // Close the database.
            mDatabaseHelper.close();
        }
    }

    /**
     * Update event list and/or delete inactive events.
     *
     * @param listItem The list of updated/deleted events
     * @return Number of inserted/updated/deleted events
     */
    public int updateEvents(List<EventItem> listItem) {
        if (listItem == null || listItem.isEmpty()) {
            // No changes.
            return 0;
        }
        int rowCount = 0;
        try {
            // Get the database.
            SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();

            // Begin the transaction.
            db.beginTransaction();

            try {
                // Get all saved event IDs.
                final String sql = "SELECT " + EventContract.Columns.EVENT_ID
                        + " FROM " + EventContract.TABLE_NAME
                        + " ORDER BY " + EventContract.Columns.EVENT_ID + " ASC";
                Cursor cursor = db.rawQuery(sql, null);
                List<Long> listSavedEventID = new ArrayList<>(cursor.getCount());
                try {
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        listSavedEventID.add(cursor.getLong(0));
                        cursor.moveToNext();
                    }
                } finally {
                    cursor.close();
                }

                if (listSavedEventID.isEmpty()) {
                    // No saved events.
                    // Insert all the new events.
                    List<EventItem> listInsert = new ArrayList<>(listItem.size());
                    for (EventItem item : listItem) {

                        if (item.getTitle()!=null) {
                            listInsert.add(item);
                        }
                    }

                    rowCount = insert(db, listInsert);
                } else {
                    // There are saved events.
                    int count = listItem.size();
                    List<EventItem> listInsert = new ArrayList<>(count),
                                    listUpdate = new ArrayList<>(count);
                    List<Long>      listDelete = new ArrayList<>(count);

                    // Check events to be inserted, updated, or deleted.
                    for (EventItem item : listItem) {
                        long eventID = item.getID();
                        if (item.getTitle()!=null) {
                            boolean exists = false;
                            for (int i = 0, n = listSavedEventID.size(); i < n; i++) {
                                if (listSavedEventID.get(i) == eventID) {
                                    exists = true;
                                    listSavedEventID.remove(i);
                                    break;
                                }
                            }
                            if (exists) {
                                listUpdate.add(item);
                            } else {
                                listInsert.add(item);
                            }
                        } else {
                            listDelete.add(eventID);
                        }
                    }

                    // Insert the new events.
                    rowCount += insert(db, listInsert);

                    // Delete removed events.
                    rowCount += delete(db, listDelete);

                    // Update existing events.
                    rowCount += update(db, listUpdate);
                }

                // Set transaction as successful.
                db.setTransactionSuccessful();
            } finally {
                // End the transaction.
                db.endTransaction();
            }
        } finally {
            // Close the database.
            mDatabaseHelper.close();
        }
        return rowCount;
    }

    /**
     * Updated saved event image status.
     *
     * @param listImage The list of event image
     */
    public void updateSavedEventImages(List<EventImageItem> listImage) {
        try {
            // Get the database.
            SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
            long savedTime = new Date().getTime();

            for (EventImageItem item : listImage) {
                try {
                    // Set the updated values.
                    ContentValues values = new ContentValues(2);
                    values.put(EventImageContract.Columns.SAVED_AS, item.getSavedAs());
                    values.put(EventImageContract.Columns.SAVED_TIME, savedTime);

                    // Set the filters.
                    String where = EventImageContract.Columns.EVENT_ID + " = ?"
                            + " AND " + EventImageContract.Columns.ORDER_NUMBER + " = ?";
                    String[] args = {String.valueOf(item.getEventID()), String.valueOf(item.getOrderNumber())};

                    // Update the record.
                    db.update(EventImageContract.TABLE_NAME, values, where, args);
                } catch (RuntimeException ex) { }
            }
        } finally {
            // Close the database.
            mDatabaseHelper.close();
        }
    }

    /**
     * Update saved event image status.
     *
     * @param eventID The event ID
     * @param orderNumber The image's order number
     * @param savedAs The saved file name
     */
    public void updateSavedEventImage(long eventID, int orderNumber, String savedAs) {
        try {
            // Get the database.
            SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();

            // Set the updated values.
            ContentValues values = new ContentValues(2);
            values.put(EventImageContract.Columns.SAVED_AS, savedAs);
            values.put(EventImageContract.Columns.SAVED_TIME, new Date().getTime());

            // Set the filters.
            String where = EventImageContract.Columns.EVENT_ID + " = ?"
                    + " AND " + EventImageContract.Columns.ORDER_NUMBER + " = ?";
            String[] args = { String.valueOf(eventID), String.valueOf(orderNumber) };

            // Update the record.
            db.update(EventImageContract.TABLE_NAME, values, where, args);
        } finally {
            // Close the database.
            mDatabaseHelper.close();
        }
    }

    /**
     * Insert the list of events to database.
     *
     * @param db The database
     * @param listItem The list of events to be inserted
     * @return Number of inserted rows
     */
    private int insert(SQLiteDatabase db, List<EventItem> listItem) {
        int rowCount = 0;
        for (EventItem event : listItem) {
            long eventID = event.getID();

            // Insert record of the event.
            ContentValues values = new ContentValues(7);
            values.put(EventContract.Columns.EVENT_ID, eventID);
            values.put(EventContract.Columns.TITLE, event.getTitle());
            values.put(EventContract.Columns.PUBLISH_DATE, event.getPublishDateSQL());
            values.put(EventContract.Columns.START_DATE, event.getStartDateSQL());
            values.put(EventContract.Columns.END_DATE, event.getEndDateSQL());
            values.put(EventContract.Columns.DESCRIPTION, event.getDescription());
            values.put(EventContract.Columns.LAST_MODIFIED, event.getLastModified());
            values.put(EventContract.Columns.THUMBNAIL_URI, event.getThumbnailURI());

            if (db.insert(EventContract.TABLE_NAME, null, values) != -1) {
                rowCount++;
            }

            // Insert record of the event images.
            /*
            for (EventImageItem img : event.getImageList()) {
                values = new ContentValues(5);
                values.put(EventImageContract.Columns.EVENT_ID, eventID);
                values.put(EventImageContract.Columns.ORDER_NUMBER, img.getOrderNumber());
                values.put(EventImageContract.Columns.ORIGINAL_URI, img.getOriginalURI());
                values.put(EventImageContract.Columns.SAVED_AS, img.getSavedAs());
                values.put(EventImageContract.Columns.SAVED_TIME, img.getSavedTimestamp());
                db.insert(EventImageContract.TABLE_NAME, null, values);
            }*/

        }
        return rowCount;
    }

    /**
     * Delete the events from database.
     *
     * @param db The database
     * @param listEventID The list of event IDs to be deleted
     * @return Number of deleted rows
     */
    private int delete(SQLiteDatabase db, List<Long> listEventID) {
        if (listEventID == null || listEventID.isEmpty()) {
            return 0;
        }

        // Convert the list to array of strings.
        int count = listEventID.size();
        String[] args = new String[count];
        for (int i = 0; i < count; i++) {
            args[i] = String.valueOf(listEventID.get(i));
        }

        // Prepare the query params.
        StringBuilder sbParams = new StringBuilder(count * 3 - 2)
                .append("?");
        for (int i = 1; i < count; i++) {
            sbParams.append(", ?");
        }

        // Delete the events.
        String where = EventContract.Columns.EVENT_ID + " IN (" + sbParams.toString() + ")";
        db.delete(EventContract.TABLE_NAME, where, args);

        // Delete the images.
        where = EventImageContract.Columns.EVENT_ID + " IN (" + sbParams.toString() + ")";
        int rowCount = db.delete(EventImageContract.TABLE_NAME, where, args);

        // Return number of deleted rows.
        return rowCount;
    }

    /**
     * Update existing events.
     *
     * @param db The database
     * @param listItem The list of events to be updated
     * @return Number of updated rows
     */
    private int update(SQLiteDatabase db, List<EventItem> listItem) {
        if (listItem == null || listItem.isEmpty()) {
            return 0;
        }

        final String WHERE_EVENT = EventContract.Columns.EVENT_ID + " = ?";
        final String WHERE_IMAGE = EventImageContract.Columns.EVENT_ID + " = ?";

        String[] SELECT_IMAGES = {
                EventImageContract.Columns.EVENT_ID,
                EventImageContract.Columns.ORDER_NUMBER,
                EventImageContract.Columns.ORIGINAL_URI,
                EventImageContract.Columns.SAVED_AS,
                EventImageContract.Columns.SAVED_TIME
        };

        int rowCount = 0;
        for (EventItem event : listItem) {
            long eventID = event.getID();
            String[] args = { String.valueOf(eventID) };

            // Update the event record.
            ContentValues values = new ContentValues(6);
            values.put(EventContract.Columns.TITLE, event.getTitle());
            values.put(EventContract.Columns.PUBLISH_DATE, event.getPublishDateSQL());
            values.put(EventContract.Columns.START_DATE, event.getStartDateSQL());
            values.put(EventContract.Columns.END_DATE, event.getEndDateSQL());
            values.put(EventContract.Columns.DESCRIPTION, event.getDescription());
            values.put(EventContract.Columns.LAST_MODIFIED, event.getLastModified());
            values.put(EventContract.Columns.THUMBNAIL_URI, event.getThumbnailURI());
            rowCount += db.update(EventContract.TABLE_NAME, values, WHERE_EVENT, args);

            // Update the event images.
            {
                // Get the current saved records.
                Cursor cursor = db.query(EventImageContract.TABLE_NAME, SELECT_IMAGES, WHERE_IMAGE, args, null, null, EventImageContract.Columns.ORDER_NUMBER);
                List<EventImageItem> listSavedImage = new ArrayList<>(cursor.getCount());
                int colEventID     = cursor.getColumnIndex(EventImageContract.Columns.EVENT_ID),
                    colOrderNumber = cursor.getColumnIndex(EventImageContract.Columns.ORDER_NUMBER),
                    colOriginalURI = cursor.getColumnIndex(EventImageContract.Columns.ORIGINAL_URI),
                    colSavedAs     = cursor.getColumnIndex(EventImageContract.Columns.SAVED_AS),
                    colSavedTime   = cursor.getColumnIndex(EventImageContract.Columns.SAVED_TIME);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    EventImageItem img = new EventImageItem(cursor.getLong(colEventID), cursor.getInt(colOrderNumber));
                    img.setOriginalURI(cursor.getString(colOriginalURI));
                    img.setSavedAs(cursor.getString(colSavedAs));
                    img.setSavedTimestamp(cursor.getLong(colSavedTime));
                    listSavedImage.add(img);
                    cursor.moveToNext();
                }

                // Get the new records, check if there are any changes.
                List<EventImageItem> listNewImage = event.getImageList();
                boolean hasChanges = (listNewImage.size() != listSavedImage.size());
                for (EventImageItem newImage : listNewImage) {
                    String originalURI = newImage.getOriginalURI();
                    boolean exists = false;
                    for (EventImageItem savedImage : listSavedImage) {
                        if (savedImage.getOriginalURI().equals(originalURI)) {
                            newImage.setSavedAs(savedImage.getSavedAs());
                            newImage.setSavedTimestamp(savedImage.getSavedTimestamp());
                            exists = true;
                            if (savedImage.getOrderNumber() != newImage.getOrderNumber()) {
                                hasChanges = true;
                            }
                            break;
                        }
                    }
                    if (!exists) {
                        hasChanges = true;
                    }
                }

                if (hasChanges) {
                    // There are changes to the image list.
                    // Delete the current saved image data from database.
                    db.delete(EventImageContract.TABLE_NAME, WHERE_IMAGE, args);
                    // Insert the new image data.
                    for (EventImageItem img : listNewImage) {
                        values = new ContentValues(5);
                        values.put(EventImageContract.Columns.EVENT_ID, eventID);
                        values.put(EventImageContract.Columns.ORDER_NUMBER, img.getOrderNumber());
                        values.put(EventImageContract.Columns.ORIGINAL_URI, img.getOriginalURI());
                        values.put(EventImageContract.Columns.SAVED_AS, img.getSavedAs());
                        values.put(EventImageContract.Columns.SAVED_TIME, img.getSavedTimestamp());
                        db.insert(EventImageContract.TABLE_NAME, null, values);
                    }
                }
            }
        }

        // Return number of updated rows.
        return rowCount;
    }
}
