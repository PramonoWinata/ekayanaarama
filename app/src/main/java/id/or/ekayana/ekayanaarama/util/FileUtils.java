package id.or.ekayana.ekayanaarama.util;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Utils for file.
 *
 * @author Jony
 * @since 05/07/2016
 */
public class FileUtils {

    private static final String EOL = System.getProperty("line.separator");

    /**
     * Read a file from assets.
     *
     * @param context The context of the application package implementing this class.
     * @param fileName The name of the file to delete; can not contain path separators.
     * @return The content of the file.
     */
    public static String readAssetFile(Context context, String fileName) throws FileNotFoundException {
        AssetManager assetManager = context.getAssets();
        BufferedReader reader = null;
        try {
            // Initialize the reader for the asset file.
            reader = new BufferedReader(new InputStreamReader(assetManager.open(fileName)));

            // Read the file content.
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + EOL);
            }

            // Return the content.
            return sb.toString();
        } catch (FileNotFoundException e) {
            // File not found.
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Error occurred while reading file \"" + fileName + "\".");
        } finally {
            // Close the resources.
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
