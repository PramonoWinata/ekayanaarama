package id.or.ekayana.ekayanaarama.activity.page;

import android.os.Bundle;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;

/**
 *
 * @author Jony
 * @since 02/07/2016
 */
public class ContactActivity extends BasePageActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_contact);
    }
}
