package id.or.ekayana.ekayanaarama.activity.page;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;
import id.or.ekayana.ekayanaarama.app.adapter.BuddhavacanaAdapter;
import id.or.ekayana.ekayanaarama.util.FileUtils;

/**
 *
 * @author Jony
 * @since 02/07/2016
 */
public class BuddhavacanaActivity extends BasePageActivity {

    public static class BuddhavacanaArticle {
        private static final String ASSET_DIRECTORY  = "buddhavacana";
        private static final String ASSET_INDEX_FILE = "index";
        private static final String ITEM_ID    = "id",
                                    ITEM_TITLE = "title";

        /**
         * Get the index of article list.
         *
         * @param context The context
         * @return The list of articles
         */
        public static List<BuddhavacanaArticle> getIndex(Context context) {
            try {
                // Get the index from asset file.
                String fileName = ASSET_DIRECTORY + File.separator + ASSET_INDEX_FILE;
                String fileContent = FileUtils.readAssetFile(context, fileName);

                // Parse the file content to JSON, and get the article index.
                JSONArray jsonItems = new JSONObject(fileContent).getJSONArray("index");

                // Get the index.
                List<BuddhavacanaArticle> list = new ArrayList<>(jsonItems.length());
                for (int i = 0, n = jsonItems.length(); i < n; i++) {
                    JSONObject jsonItem = jsonItems.getJSONObject(i);
                    String id = jsonItem.getString(ITEM_ID),
                           title = jsonItem.getString(ITEM_TITLE);
                    list.add(new BuddhavacanaArticle(id, title));
                }

                // Return the index.
                return list;
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
                String msg = "No data found";
                throw new RuntimeException(msg, ex);
            } catch (JSONException ex) {
                ex.printStackTrace();
                String msg = "Error loading data";
                throw new RuntimeException(msg, ex);
            }
        }

        private final String id, title;

        private BuddhavacanaArticle(String id, String title) {
            this.id = id;
            this.title = title;
        }

        public String getID() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        @Override
        public String toString() {
            return id.substring(1) + ". " + title;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_buddhavacana);
        ListView listView = (ListView) findViewById(R.id.list);

        // Get the data.
        List<BuddhavacanaArticle> listArticle;
        try {
            listArticle = BuddhavacanaArticle.getIndex(this);
        } catch (RuntimeException ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }

        // Set the adapter.
        BuddhavacanaAdapter adapter = new BuddhavacanaAdapter(this, android.R.layout.simple_list_item_1, listArticle);
        listView.setAdapter(adapter);

        // Set the click listener.
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item.
                BuddhavacanaArticle item = (BuddhavacanaArticle) view.getTag();

                // Create intent with the selected item as data.
                Intent intent = new Intent(BuddhavacanaActivity.this, BuddhavacanaDetailActivity.class);
                intent.putExtra(BuddhavacanaDetailActivity.EXTRA_ID, item.getID());
                intent.putExtra(BuddhavacanaDetailActivity.EXTRA_TITLE, item.toString());

                // Start the detail activity.
                startActivity(intent);
            }
        });
    }
}
