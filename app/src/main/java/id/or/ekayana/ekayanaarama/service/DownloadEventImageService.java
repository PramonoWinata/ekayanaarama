package id.or.ekayana.ekayanaarama.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import id.or.ekayana.ekayanaarama.app.AppConfig;
import id.or.ekayana.ekayanaarama.app.exception.NetworkNotConnectedException;
import id.or.ekayana.ekayanaarama.app.file.EventImageDirectory;
import id.or.ekayana.ekayanaarama.data.db.EventDA;
import id.or.ekayana.ekayanaarama.data.download.Downloader;
import id.or.ekayana.ekayanaarama.model.EventImageItem;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests
 * to download an event image and save the file to local storage.
 */
public class DownloadEventImageService extends IntentService {

    private static final String CANONICAL_NAME = DownloadEventImageService.class.getCanonicalName();
    public static final String ACTION_REPORT_STATUS = CANONICAL_NAME + ".action.REPORT_STATUS";
    public static final String EXTRA_EVENT_ID       = CANONICAL_NAME + ".extra.EVENT_ID",
                               EXTRA_ORDER_NUMBERS  = CANONICAL_NAME + ".extra.ORDER_NUMS",
                               EXTRA_FILE_NAMES     = CANONICAL_NAME + ".extra.FILE_NAMES",
                               EXTRA_SUCCESS        = CANONICAL_NAME + ".extra.SUCCESS",
                               EXTRA_MESSAGE        = CANONICAL_NAME + ".extra.MESSAGE";

    private static final String ACTION_DOWNLOAD = "id.or.ekayana.ekayanaarama.service.action.DOWNLOAD";

    /**
     * Starts the service
     * @param context The context
     * @param eventID The event ID of the images to be downloaded
     * @param orderNumbers The order numbers of the event images to be downloaded
     */
    public static void start(Context context, long eventID, int[] orderNumbers) {
        System.out.println("Starting service " + DownloadEventImageService.class.getSimpleName() + "...");
        Intent intent = new Intent(context, DownloadEventImageService.class);
        intent.setAction(ACTION_DOWNLOAD);
        intent.putExtra(EXTRA_EVENT_ID, eventID);
        intent.putExtra(EXTRA_ORDER_NUMBERS, orderNumbers);
        context.startService(intent);
    }

    public DownloadEventImageService() {
        super("DownloadEventImageService");
    }

    /**
     * Handles requests to the service.
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            System.out.println("Service " + this.getClass().getSimpleName() + " started (action=" + intent.getAction() + ").");
            if (ACTION_DOWNLOAD.equals(intent.getAction())) {
                handleActionDownload(intent);
            }
        }
    }

    /**
     * Handles download action.
     * @param intent
     */
    private void handleActionDownload(Intent intent) {
        // Get the intent data.
        long eventID = intent.getLongExtra(EXTRA_EVENT_ID, 0);
        int[] orderNumbers = intent.getIntArrayExtra(EXTRA_ORDER_NUMBERS);

        // Get the event images to be downloaded.
        EventDA eventDA = new EventDA(this);
        List<EventImageItem> listImage = eventDA.getEventImageList(eventID, orderNumbers);

        // Get downloader.
        Downloader downloader = new Downloader(this, EventImageDirectory.get(this));

        // Download the requested event images.
        List<EventImageItem> listSavedImage = new ArrayList<>(listImage.size());
        try {
            for (EventImageItem imageItem : listImage) {
                int orderNumber = imageItem.getOrderNumber();
                String originalURI = imageItem.getOriginalURI(),
                       savedAs = imageItem.getSavedAs();

                Log.d("image", imageItem.getOriginalURI());

                if (savedAs == null || savedAs.isEmpty()) {
                    // Generate saved file name.
                    int random = (int) (Math.round(Math.random()) * 89 + 10);
                    String seed = String.valueOf(System.currentTimeMillis());
                    seed = seed.substring(seed.length() - 8) + random;
                    savedAs = imageItem.getEventID() + "_" + orderNumber + "_" + originalURI.length() + seed;
                }
                try {
                    // Download the image file.
                    savedAs = downloader.download(AppConfig.SERVER_HOST + originalURI, savedAs);

                    // Add to list of saved images.
                    imageItem.setSavedAs(savedAs);
                    listSavedImage.add(imageItem);

                    // Send success status.
                    sendSuccess(eventID, orderNumber, savedAs);
                } catch (FileNotFoundException ex) {
                    sendError(eventID, orderNumber, "File not found on server");
                } catch (SocketException ex) {
                    sendError(eventID, orderNumber, "Connection to server failed");
                } catch (IOException ex) {
                    sendError(eventID, orderNumber, "Saving downloaded file failed");
                } catch (RuntimeException ex) {
                    sendError(eventID, orderNumber, "An error occurred");
                }
            }
        } catch (NetworkNotConnectedException ex) {
            sendError(eventID, orderNumbers, "Not connected to network");
        }

        // Update status of the saved image items.
        if (!listSavedImage.isEmpty()) {
            eventDA.updateSavedEventImages(listSavedImage);
        }
    }

    /**
     * Send result of a running action as successful.
     * @param eventID The event ID
     * @param orderNumber The image' order number
     * @param savedAs The file name of the downloaded image
     */
    private void sendSuccess(long eventID, int orderNumber, String savedAs) {
        int[] orderNumbers = { orderNumber };
        String[] fileNames = { savedAs };
        sendSuccess(eventID, orderNumbers, fileNames);
    }

    /**
     * Send result of a running action as successful.
     * @param eventID The event ID
     * @param orderNumbers The images' order numbers
     * @param savedAs The file names of the downloaded images
     */
    private void sendSuccess(long eventID, int[] orderNumbers, String[] savedAs) {
        Intent intent = new Intent(ACTION_REPORT_STATUS)
                .putExtra(EXTRA_EVENT_ID, eventID)
                .putExtra(EXTRA_ORDER_NUMBERS, orderNumbers)
                .putExtra(EXTRA_FILE_NAMES, savedAs)
                .putExtra(EXTRA_SUCCESS, true);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Send result of a running action as failed.
     * @param eventID The event ID
     * @param orderNumber The images' order numbers
     * @param message The message to be displayed
     */
    private void sendError(long eventID, int orderNumber, String message) {
        int[] orderNumbers = { orderNumber };
        sendError(eventID, orderNumbers, message);
    }

    /**
     * Send result of a running action as failed.
     * @param eventID The event ID
     * @param orderNumbers The images' order numbers
     * @param message The message to be displayed
     */
    private void sendError(long eventID, int[] orderNumbers, String message) {
        Intent intent = new Intent(ACTION_REPORT_STATUS)
                .putExtra(EXTRA_EVENT_ID, eventID)
                .putExtra(EXTRA_ORDER_NUMBERS, orderNumbers)
                .putExtra(EXTRA_SUCCESS, false)
                .putExtra(EXTRA_MESSAGE, message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
