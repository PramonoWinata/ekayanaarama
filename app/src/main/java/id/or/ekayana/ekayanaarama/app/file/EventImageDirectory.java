package id.or.ekayana.ekayanaarama.app.file;

import android.content.Context;

import java.io.File;

/**
 * {@code File} which represents directory for event images.
 *
 * @author Jony
 * @since 28/12/2016
 */

public class EventImageDirectory extends File {
    private static final String DIRECTORY_NAME = "event_img";

    public static EventImageDirectory get(Context context) {
        return new EventImageDirectory(context);
    }

    private EventImageDirectory(Context context) {
        super(context.getFilesDir(), DIRECTORY_NAME);
    }
}
