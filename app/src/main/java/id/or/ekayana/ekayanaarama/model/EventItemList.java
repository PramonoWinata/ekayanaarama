package id.or.ekayana.ekayanaarama.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventItemList {

    @SerializedName("items")
    private List<EventItem> listEvent;

    public List<EventItem> getListEvent() {
        return listEvent;
    }

    public void setListEvent(List<EventItem> listEvent) {
        this.listEvent = listEvent;
    }
}
