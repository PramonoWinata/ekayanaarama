package id.or.ekayana.ekayanaarama.activity.page;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;
import id.or.ekayana.ekayanaarama.app.adapter.MenuAdapter;
import id.or.ekayana.ekayanaarama.model.MenuItem;

/**
 *
 * @author Jony
 * @since 02/07/2016
 */
public class MoreMenuActivity extends BasePageActivity {

    private static abstract class Menu {
        private static final int MENU_PREFERENCES = 1,
                                 MENU_ABOUT_APP   = 2;

        public static ArrayList<MenuItem> getItems(Context ctx) {
            ArrayList<MenuItem> list = new ArrayList<>(7);
            list.add(new MenuItem(MENU_PREFERENCES, ctx.getString(R.string.activity_preferences), PreferencesActivity.class));
            list.add(new MenuItem(MENU_ABOUT_APP, ctx.getString(R.string.activity_about_app), AppInfoActivity.class));
            return list;
        }
    }

    private ListView mMenuListView = null;

    private ListView getMainMenuListView() {
        if (mMenuListView == null) {
            mMenuListView = (ListView) findViewById(R.id.menu_list);
        }
        return mMenuListView;
    }

    private void setMenu() {
        // Set the main menu items.
        ArrayList<MenuItem> listMenuItem = Menu.getItems(this);
        MenuAdapter adapter = new MenuAdapter(this, android.R.layout.simple_list_item_1, listMenuItem);
        mMenuListView.setAdapter(adapter);

        // Set the click listener.
        mMenuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MenuItem menuItem = (MenuItem) view.getTag(R.id.menu_item);
                Intent intent = new Intent(MoreMenuActivity.this, menuItem.getActivityClass());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_more);
        mMenuListView = (ListView) findViewById(R.id.menu_list);

        // Load main menu.
        setMenu();
    }
}
