package id.or.ekayana.ekayanaarama.app.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Base adapter for custom adapter.
 *
 * @author Jony
 * @since 26/12/2016
 */
public abstract class CustomAdapter<T> extends BaseAdapter {

    private final Context mContext;
    private final LayoutInflater mInflater;
    private final List<T> mListItem;

    public Context getContext() {
        return mContext;
    }

    public LayoutInflater getInflater() {
        return mInflater;
    }

    public List<T> getItemList() {
        return mListItem;
    }

    protected View inflate(@LayoutRes int resource, @Nullable ViewGroup root) {
        return mInflater.inflate(resource, root);
    }

    /**
     * Constructor
     *
     * @param context The current context.
     * @param listItem The list of items to represent.
     */
    public CustomAdapter(Context context, List<T> listItem) {
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mListItem = listItem;
    }

    @Override
    public int getCount() {
        return mListItem.size();
    }

    @Override
    public T getItem(int position) {
        return mListItem.get(position);
    }
}
