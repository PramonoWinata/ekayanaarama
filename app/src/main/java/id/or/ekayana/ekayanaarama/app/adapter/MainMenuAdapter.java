package id.or.ekayana.ekayanaarama.app.adapter;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.model.MenuItem;

/**
 * Adapter for main menu.
 *
 * @author Jony
 * @since 02/07/2016
 */
public class MainMenuAdapter extends CustomAdapter<MenuItem> {

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param listItem The list of menu items to represent.
     */
    public MainMenuAdapter(Context context, List<MenuItem> listItem) {
        super(context, listItem);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getID();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // Get the item of the specified position.
        MenuItem menuItem = getItem(position);

        // Generate the view.
        if (view == null) {
            view = inflate(R.layout.menu_grid_item, null);
        }
        ImageView imgIcon = (ImageView) view.findViewById(R.id.icon);
        if (menuItem.hasIcon()) {
            if (Build.VERSION.SDK_INT < 21) {
                imgIcon.setImageDrawable(getContext().getResources().getDrawable(menuItem.getIconResourceID()));
            } else {
                imgIcon.setImageDrawable(getContext().getDrawable(menuItem.getIconResourceID()));
            }
        } else {
            imgIcon.setVisibility(View.GONE);
        }
        TextView textView = (TextView) view.findViewById(R.id.label);
        textView.setText(menuItem.getName());

        // Set the item to the view's tags.
        view.setTag(R.id.menu_item, menuItem);

        // Return the view.
        return view;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItem(position).isEnabled();
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }
}
