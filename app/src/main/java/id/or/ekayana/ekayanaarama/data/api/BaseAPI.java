package id.or.ekayana.ekayanaarama.data.api;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import id.or.ekayana.ekayanaarama.app.AppConfig;
import id.or.ekayana.ekayanaarama.app.exception.NetworkNotConnectedException;
import id.or.ekayana.ekayanaarama.util.ConnectionUtils;
import id.or.ekayana.ekayanaarama.util.StringUtils;

/**
 * Base class to handle API requests.
 *
 * @author Jony
 * @since  28/11/2016
 */

public abstract class BaseAPI {
    private static final int CONNECTION_TIMEOUT_MILLIS = 10 * 1000;
    private static final String CHARSET = "UTF-8";
    private static final String EOL = System.getProperty("line.separator");

    private final Context mContext;

    protected BaseAPI(Context context) {
        this.mContext = context;
    }

    protected Context getContext() {
        return mContext;
    }

    protected String sendRequest(String uri, Map<String, String> parameters) throws IOException {
        // Check network connection.
        if (!ConnectionUtils.isNetworkAvailable(mContext)) {
            throw new NetworkNotConnectedException("Not connected to network.");
        }

        // Generate the request parameters, if any.
        if (parameters != null && !parameters.isEmpty()) {
            StringBuilder sb = new StringBuilder(parameters.size() * 10);
            boolean isFirst = true;
            for (String key : parameters.keySet()) {
                if (isFirst) {
                    sb.append("?");
                    isFirst = false;
                } else {
                    sb.append("&");
                }
                sb.append(key).append("=").append(StringUtils.encodeURIComponent(parameters.get(key), CHARSET));
            }
            uri += sb.toString();
        }

        // Open the connection.
        HttpURLConnection connection = (HttpURLConnection) new URL(AppConfig.SERVER_HOST + uri).openConnection();
        connection.setConnectTimeout(CONNECTION_TIMEOUT_MILLIS);
        connection.setRequestProperty("Accept-Charset", CHARSET);

        // Send the request and get the response.
        BufferedReader reader = null;
        try {
            InputStream inputStream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + EOL);
            }
            return sb.toString();
        } catch (IOException ex) {
            System.out.println(ex);
            throw ex;
        } finally {
            if (reader != null) {
                reader.close();
            }
            connection.disconnect();
        }
    }

    protected String[] toStringArray(JSONArray jsonArray) throws JSONException {
        int length = jsonArray.length();
        String[] s = new String[length];
        for (int i = 0; i < length; i++) {
            s[i] = jsonArray.getString(i);
        }
        return s;
    }
}
