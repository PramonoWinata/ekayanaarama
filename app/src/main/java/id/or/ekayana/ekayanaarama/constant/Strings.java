package id.or.ekayana.ekayanaarama.constant;

/**
 * String constants.
 *
 * @author Jony
 * @since 02/07/2016
 */
public abstract class Strings {
    public static final String DASH = "-",
                               EMPTY = "",
                               SPACE = " ";
}
