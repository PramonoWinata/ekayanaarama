package id.or.ekayana.ekayanaarama.data.db.contract;

import android.provider.BaseColumns;

/**
 * @author Jony
 * @since 04/12/2016
 */

public interface EventContract {
    public static final String TABLE_NAME = "events";
    public static interface Columns extends BaseColumns {
        public static final String
                EVENT_ID        = "event_id",
                TITLE           = "title",
                PUBLISH_DATE    = "publish_date",
                START_DATE      = "start_date",
                END_DATE        = "end_date",
                DESCRIPTION     = "description",
                LAST_MODIFIED   = "last_modified",
                THUMBNAIL_URI   = "thumbnail_uri";
    }
}
