package id.or.ekayana.ekayanaarama.app.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.model.MenuItem;

/**
 * Adapter for main menu.
 *
 * @author Jony
 * @since 02/07/2016
 */
public class MenuAdapter extends ArrayAdapter<MenuItem> {
    private final List<MenuItem> mListMenuItem;

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param resource The resource ID for a layout file containing a TextView to use when
     *                 instantiating views.
     * @param listMenuItem  The list of menu items to represent in the ListView.
     */
    public MenuAdapter(Context context, int resource, List<MenuItem> listMenuItem) {
        super(context, resource, listMenuItem);
        mListMenuItem = listMenuItem;
    }

    @Override
    public long getItemId(int position) {
        MenuItem item = getItem(position);
        return item.getID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the  view.
        View view = super.getView(position, convertView, parent);
        // Get the item of the specified position.
        MenuItem item = getItem(position);
        // Set the item to the view's tags.
        view.setTag(R.id.menu_item, item);
        // Return the view.
        return view;
    }
}
