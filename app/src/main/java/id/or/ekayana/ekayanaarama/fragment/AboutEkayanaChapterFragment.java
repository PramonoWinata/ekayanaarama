package id.or.ekayana.ekayanaarama.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.or.ekayana.ekayanaarama.R;

/**
 * Content tab for "About Ekayana".
 *
 * @author Jony
 * @since 18/09/2016
 */
public class AboutEkayanaChapterFragment extends Fragment {

    private static final String ARG_CONTENT_RES_ID = "content";

    /**
     * Get new instance of <code>AboutEkayanaChapterFragment</code>.
     * @param resId Resource ID for the content
     * @return
     */
    public static AboutEkayanaChapterFragment newInstance(int resId) {
        AboutEkayanaChapterFragment fragment = new AboutEkayanaChapterFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CONTENT_RES_ID, resId);
        fragment.setArguments(args);
        return fragment;
    }

    private String mContent;
    private TextView mTextView;

    public AboutEkayanaChapterFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String content = getString(getArguments().getInt(ARG_CONTENT_RES_ID));
        View view = inflater.inflate(R.layout.fragment_about_ekayana_chapter, container, false);
        mTextView = (TextView) view.findViewById(R.id.content);
        mTextView.setText(Html.fromHtml(content));
        return view;
    }

}
