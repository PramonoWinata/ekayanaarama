package id.or.ekayana.ekayanaarama.activity.page;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;
import id.or.ekayana.ekayanaarama.app.adapter.BuddhavacanaAdapter;
import id.or.ekayana.ekayanaarama.util.FileUtils;

/**
 *
 * @author Jony
 * @since 12/08/2016
 */
public class BuddhavacanaDetailActivity extends BasePageActivity {

    // Extra names for intent calling this activity.
    public static final String EXTRA_ID    = BuddhavacanaDetailActivity.class.getCanonicalName() + ".ID",
                               EXTRA_TITLE = BuddhavacanaDetailActivity.class.getCanonicalName() + ".TITLE";

    private static class BuddhavacanaArticleDetail {
        private static final String ASSET_DIRECTORY  = "buddhavacana";
        private static final String ITEM_PARAGRAPHS  = "paragraphs",
                                    ITEM_SOURCE      = "source",
                                    ITEM_PARAGRAPH_CONTENT     = "content",
                                    ITEM_PARAGRAPH_QUOTE_STYLE = "quote_style";

        /**
         * Get the detail of an article.
         *
         * @param context The context
         * @param id The ID of the article
         * @return The detail
         */
        public static BuddhavacanaArticleDetail get(Context context, String id) {
            try {
                // Get the file content from asset.
                String fileName = ASSET_DIRECTORY + File.separator + id;
                String fileContent = FileUtils.readAssetFile(context, fileName);

                // Parse the file content to JSON.
                JSONObject jsonRoot = new JSONObject(fileContent);

                // Get the content.
                JSONArray jsonParagraphs = jsonRoot.getJSONArray(ITEM_PARAGRAPHS);
                String source = jsonRoot.getString(ITEM_SOURCE);
                Paragraph[] paragraphs = new Paragraph[jsonParagraphs.length()];
                for (int i = 0, n = jsonParagraphs.length(); i < n; i++) {
                    JSONObject jsonItem = jsonParagraphs.getJSONObject(i);
                    paragraphs[i] = new Paragraph(jsonItem.getString(ITEM_PARAGRAPH_CONTENT), jsonItem.getBoolean(ITEM_PARAGRAPH_QUOTE_STYLE));
                }

                // Return the detail.
                return new BuddhavacanaArticleDetail(paragraphs, source);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
                String msg = "No data found";
                throw new RuntimeException(msg, ex);
            } catch (JSONException ex) {
                ex.printStackTrace();
                String msg = "Error loading data";
                throw new RuntimeException(msg, ex);
            }
        }

        private final Paragraph[] content;
        private final String source;

        private BuddhavacanaArticleDetail(Paragraph[] content, String source) {
            this.content = content;
            this.source = source;
        }

        public Paragraph[] getContent() {
            return content;
        }

        public String getSource() {
            return source;
        }

        public boolean isAllQuote() {
            for (Paragraph p : content) {
                if (!p.isQuoteStyle()) {
                    return false;
                }
            }
            return true;
        }
    }

    private static class Paragraph {
        String content;
        boolean isQuoteStyle;

        public Paragraph(String content) {
            this.content = content;
            this.isQuoteStyle = false;
        }

        public Paragraph(String content, boolean isQuoteStyle) {
            this.content = content;
            this.isQuoteStyle = isQuoteStyle;
        }

        public String getContent() {
            return content;
        }

        public boolean isQuoteStyle() {
            return isQuoteStyle;
        }
    }

    private static final String INDENT_STRING = "&nbsp;&nbsp;&nbsp;&nbsp;";

    private LinearLayout mLayoutContent = null;
    private TextView mTextSource = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_buddhavacana_detail);
        mLayoutContent = (LinearLayout) findViewById(R.id.content);
        mTextSource = (TextView) findViewById(R.id.source);

        // Get the data from the intent.
        Intent intent = getIntent();
        String itemID = intent.getStringExtra(EXTRA_ID),
                title = intent.getStringExtra(EXTRA_TITLE);

        // Set the activity title.
        setTitle(title);

        // Load the detail.
        loadDetail(itemID);
    }

    private void loadDetail(String id) {
        // Get the detail of the article.
        BuddhavacanaArticleDetail detail;
        try {
            detail = BuddhavacanaArticleDetail.get(this, id);
        } catch (RuntimeException ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }
        Paragraph[] paragraphs = detail.getContent();

        // Calculate 16 px.
        int px16 = (int) Math.ceil(16 * getResources().getDisplayMetrics().density);

        // Set the content.
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        for (int i = 0, n = paragraphs.length; i < n; i++) {
            Paragraph paragraph = paragraphs[i];
            boolean isQuoteStyle = paragraph.isQuoteStyle();
            String content = (isQuoteStyle)
                    ? paragraph.getContent()
                    : INDENT_STRING + paragraph.getContent();

            // Determine the text paddings.
            int paddingLeft = (isQuoteStyle && !detail.isAllQuote()) ? px16 : 0,
                paddingTop  = (i > 0) ? px16 : 0;

            // Create {@code TextView} for each content paragraph.
            TextView textContent = new TextView(this);
            textContent.setLayoutParams(layoutParams);
            textContent.setPadding(paddingLeft, paddingTop, 0, 0);
            textContent.setText(Html.fromHtml(content));
            mLayoutContent.addView(textContent);
        }

        // Set the source.
        mTextSource.setText(detail.getSource());
    }
}
