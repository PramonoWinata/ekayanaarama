package id.or.ekayana.ekayanaarama.data.retrofit;

import id.or.ekayana.ekayanaarama.model.EventItemList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface EkayanaEndpoint {

    @GET("api/get_event")
    Call<EventItemList> getEventList(@Query("timestamp") long timestamp);
}
