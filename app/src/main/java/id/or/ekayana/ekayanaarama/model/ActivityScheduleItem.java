package id.or.ekayana.ekayanaarama.model;

import id.or.ekayana.ekayanaarama.util.StringUtils;

public class ActivityScheduleItem {
    private final String day,
            time,
            location;

    public ActivityScheduleItem(String day, String time) {
        this.day = day;
        this.time = time;
        this.location = null;
    }

    public ActivityScheduleItem(String day, String time, String location) {
        this.day = day;
        this.time = time;
        this.location = location;
    }

    public String getDay() {
        return day;
    }

    public String getTime() {
        return time;
    }

    public String getLocation() {
        return location;
    }

    public boolean hasLocation() {
        return !StringUtils.isNullOrEmpty(location);
    }
}
