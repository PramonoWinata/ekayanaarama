package id.or.ekayana.ekayanaarama.util;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import id.or.ekayana.ekayanaarama.constant.Strings;

/**
 * Utils for date-related data.
 *
 * @author Jony
 * @since 18/12/2016
 */

public class DateUtils {

    public static Date parseSqlDate(String sqlDate) {
        if (sqlDate == null || sqlDate.isEmpty()) {
            return null;
        }
        if (sqlDate.contains(Strings.SPACE)) {
            String[] parts = sqlDate.split(Strings.SPACE);
            sqlDate = parts[0];
        }
        String[] parts = sqlDate.split(Strings.DASH);
        if (parts.length != 3) {
            throw new RuntimeException("Invalid SQL date format.");
        }
        int year  = Integer.parseInt(parts[0]),
            month = Integer.parseInt(parts[1]) - 1,
            day   = Integer.parseInt(parts[2]);
        Calendar c = Calendar.getInstance();
        c.set(year, month, day, 0, 0, 0);
        return c.getTime();
    }
}
