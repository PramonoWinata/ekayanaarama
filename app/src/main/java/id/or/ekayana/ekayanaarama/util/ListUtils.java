package id.or.ekayana.ekayanaarama.util;

import java.util.List;

import id.or.ekayana.ekayanaarama.constant.Strings;

/**
 * Utils for List or arrays.
 *
 * @author Jony
 * @since 17/12/2016
 */

public class ListUtils {

    public static final String joinToString(List<String> list, String separator) {
        if (list == null) {
            return null;
        } else if (list.isEmpty()) {
            return Strings.EMPTY;
        }
        int size = list.size();
        String first = list.get(0);
        if (size == 1) {
            return first;
        }
        StringBuilder sb = new StringBuilder((first.length() + separator.length()) * size);
        sb.append(first);
        for (int i = 1; i < size; i++) {
            sb.append(separator);
            sb.append(list.get(i));
        }
        return sb.toString();
    }

    public static final String joinToString(String[] arr, String separator) {
        if (arr == null) {
            return null;
        } else if (arr.length == 0) {
            return Strings.EMPTY;
        }
        String first = arr[0];
        if (arr.length == 1) {
            return first;
        }
        StringBuilder sb = new StringBuilder((first.length() + separator.length()) * arr.length);
        sb.append(first);
        for (int i = 1; i < arr.length; i++) {
            sb.append(separator);
            sb.append(arr[i]);
        }
        return sb.toString();
    }

    public static final String joinToString(int[] arr, String separator) {
        if (arr == null) {
            return null;
        } else if (arr.length == 0) {
            return Strings.EMPTY;
        }
        String first = String.valueOf(arr[0]);
        if (arr.length == 1) {
            return first;
        }
        StringBuilder sb = new StringBuilder((first.length() + separator.length()) * arr.length);
        sb.append(first);
        for (int i = 1; i < arr.length; i++) {
            sb.append(separator);
            sb.append(arr[i]);
        }
        return sb.toString();
    }

    public static final int[] toArray(List<Integer> list) {
        if (list == null) {
            return null;
        } else if (list.isEmpty()) {
            return new int[0];
        }
        int size = list.size();
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            int v = list.get(i);
            arr[i] = v;
        }
        return arr;
    }
}
