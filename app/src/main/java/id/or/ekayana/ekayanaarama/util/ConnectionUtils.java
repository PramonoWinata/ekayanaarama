package id.or.ekayana.ekayanaarama.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author Jony
 * @since 13/11/2016
 */

public class ConnectionUtils {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager mgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo n = mgr.getActiveNetworkInfo();
        return (n != null && n.isConnected());
    }
}
