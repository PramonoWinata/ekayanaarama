package id.or.ekayana.ekayanaarama.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class EventItemSugar extends SugarRecord {

    @SerializedName("id")
    private long id;
    @SerializedName("isActiveRecord")
    private boolean isActiveRecord;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("publishDate")
    private String  publishDate;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("endDate")
    private String endDate;
    @SerializedName("timestamp")
    private long lastModified;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isActiveRecord() {
        return isActiveRecord;
    }

    public void setActiveRecord(boolean activeRecord) {
        isActiveRecord = activeRecord;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }
}
