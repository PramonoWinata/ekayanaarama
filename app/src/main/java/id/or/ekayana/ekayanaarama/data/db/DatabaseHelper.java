package id.or.ekayana.ekayanaarama.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import id.or.ekayana.ekayanaarama.data.db.contract.EventContract;
import id.or.ekayana.ekayanaarama.data.db.contract.EventImageContract;

/**
 * @author Jony
 * @since 04/12/2016
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1; // Increment this if the database schema is changed.
    private static final String DB_NAME = "ekayana.db";

    /**
     * Creates a helper object to create, open, and/or manage a database.
     *
     * @param context to use to open or create the database
     */
    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_EVENTS = "CREATE TABLE " + EventContract.TABLE_NAME + " ( "
                + EventContract.Columns._ID + " INTEGER PRIMARY KEY, "
                + EventContract.Columns.EVENT_ID + " INTEGER NOT NULL, "
                + EventContract.Columns.TITLE + " TEXT NOT NULL, "
                + EventContract.Columns.PUBLISH_DATE + " TEXT NOT NULL, "
                + EventContract.Columns.START_DATE + " TEXT NOT NULL, "
                + EventContract.Columns.END_DATE + " TEXT NOT NULL, "
                + EventContract.Columns.DESCRIPTION + " TEXT, "
                + EventContract.Columns.LAST_MODIFIED + " INTEGER NOT NULL, "
                + EventContract.Columns.THUMBNAIL_URI + " TEXT "
                + ") ";
        final String SQL_CREATE_EVENT_IMAGES = "CREATE TABLE " + EventImageContract.TABLE_NAME + " ( "
                + EventImageContract.Columns._ID + " INTEGER PRIMARY KEY, "
                + EventImageContract.Columns.EVENT_ID + " INTEGER NOT NULL, "
                + EventImageContract.Columns.ORDER_NUMBER + " INTEGER NOT NULL, "
                + EventImageContract.Columns.ORIGINAL_URI + " TEXT NOT NULL, "
                + EventImageContract.Columns.SAVED_AS + " TEXT, "
                + EventImageContract.Columns.SAVED_TIME + " INTEGER "
                + ") ";
        db.execSQL(SQL_CREATE_EVENTS);
        db.execSQL(SQL_CREATE_EVENT_IMAGES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so simply to discard the data and start over.
        db.execSQL("DROP TABLE IF EXISTS " + EventContract.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + EventImageContract.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Same action as upgrade.
        onUpgrade(db, oldVersion, newVersion);
    }
}
