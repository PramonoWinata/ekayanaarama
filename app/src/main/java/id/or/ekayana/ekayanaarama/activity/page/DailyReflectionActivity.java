package id.or.ekayana.ekayanaarama.activity.page;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;
import id.or.ekayana.ekayanaarama.constant.Strings;
import id.or.ekayana.ekayanaarama.util.FileUtils;

/**
 * Shows daily reflection based on the current date.
 *
 * @author Jony
 * @since 02/07/2016
 */
public class DailyReflectionActivity extends BasePageActivity {

    /**
     * Daily reflection item.
     */
    private static class DailyReflection {
        private static final String ASSET_DIRECTORY = "daily_reflections";
        private static final String ITEM_CONTENT    = "content",
                                    ITEM_SOURCE     = "source";

        /**
         * Get today's daily reflection item.
         *
         * @param context The context
         * @return The item
         */
        public static DailyReflection getItem(Context context) {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();

            // Get today's date & month.
            String[] parts = new SimpleDateFormat("yyyy-MM-dd").format(date).split("-");
            String   day   = parts[2],
                     month = parts[1];

            try {
                // Get the file content from assets.
                String fileName = ASSET_DIRECTORY + File.separator + month;
                String fileContent = FileUtils.readAssetFile(context, fileName);

                // Parse the file content to JSON, and get the item for today.
                JSONObject item = new JSONObject(fileContent).getJSONObject(day);
                JSONArray itemContent = item.getJSONArray(ITEM_CONTENT);

                // Get the item data.
                String   title    = generateTitle(calendar),
                         source   = item.getString(ITEM_SOURCE);
                String[] contents = new String[itemContent.length()];
                for (int i = 0, n = itemContent.length(); i < n; i++) {
                    contents[i] = itemContent.getString(i);
                }

                // Create and return new instance.
                return new DailyReflection(title, source, contents);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
                String msg = "No data found";
                throw new RuntimeException(msg, ex);
            } catch (JSONException ex) {
                ex.printStackTrace();
                String msg = "Error loading data";
                throw new RuntimeException(msg, ex);
            }
        }

        /**
         * Generate title for an item.
         *
         * @param calendar
         * @return
         */
        private static String generateTitle(Calendar calendar) {
            String[] monthNames = { "Januari", "Februari", "Maret", "April", "Mei", "Juni",
                                    "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
            int month = calendar.get(Calendar.MONTH),
                day   = calendar.get(Calendar.DAY_OF_MONTH);
            return day + Strings.SPACE + monthNames[month];
        }

        private final String   header;
        private final String   footer;
        private final String[] content;

        private DailyReflection(String header, String footer, String[] content) {
            this.header  = header;
            this.footer  = footer;
            this.content = content;
        }

        public String getHeader() {
            return header;
        }

        public String getFooter() {
            return footer;
        }

        public String[] getContent() {
            return content;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_daily_reflection);
        TextView textHeader  = (TextView) findViewById(R.id.header),
                 textFooter  = (TextView) findViewById(R.id.footer);
        LinearLayout content = (LinearLayout) findViewById(R.id.content);

        // Get the data.
        DailyReflection data;
        try {
            data = DailyReflection.getItem(this);
        } catch (RuntimeException ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }

        // Set the header and footer.
        textHeader.setText(Html.fromHtml(data.getHeader()));
        textFooter.setText(Html.fromHtml(data.getFooter()));

        // Set the layout params for each content paragraph.
        int marginTopPx = getResources().getDimensionPixelSize(R.dimen.text_margin);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, marginTopPx, 0, 0);

        // Set the content.
        for (String s : data.getContent()) {
            // Create {@code TextView} for each content paragraph.
            TextView textContent = new TextView(this);
            textContent.setLayoutParams(layoutParams);
            textContent.setText(Html.fromHtml(s));
            content.addView(textContent);
        }
    }
}
