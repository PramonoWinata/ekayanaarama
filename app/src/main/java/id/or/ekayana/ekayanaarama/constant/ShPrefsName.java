package id.or.ekayana.ekayanaarama.constant;

/**
 * @author Jony
 * @since 01/07/2016
 */
public abstract class ShPrefsName {

    public abstract static class Key {
        public static final String
                FIRST_RUN = "firstRun",
                LAST_RUN_DATE = "lastRunDate",
                LAST_RUN_TIMESTAMP = "lastRunTimestamp";
    }
}
