package id.or.ekayana.ekayanaarama.model;

/**
 * Event image item.
 *
 * @author Jony
 * @since 18/12/2016
 */

//need to remove everything else except for event id and order number
public class EventImageItem {
    private final long eventID;
    private int     orderNumber;
    private String  originalURI,
                    savedAs;
    private long    savedTimestamp;

    public EventImageItem(long eventID, int orderNumber) {
        this.eventID = eventID;
        this.orderNumber = orderNumber;
        this.originalURI = null;
        this.savedAs = null;
        this.savedTimestamp = 0;
    }

    public long getEventID() {
        return eventID;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public String getOriginalURI() {
        return originalURI;
    }

    public String getSavedAs() {
        return savedAs;
    }

    public long getSavedTimestamp() {
        return savedTimestamp;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public void setOriginalURI(String originalURI) {
        this.originalURI = originalURI;
    }

    public void setSavedAs(String savedAs) {
        this.savedAs = savedAs;
    }

    public void setSavedTimestamp(long timestamp) {
        this.savedTimestamp = timestamp;
    }

    public boolean equals(EventImageItem o) {
        return (o != null && o.eventID == this.eventID && o.originalURI.equals(this.originalURI));
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o.getClass().equals(this.getClass())) {
            EventImageItem other = (EventImageItem) o;
            return (other.eventID == this.eventID && other.originalURI.equals(this.originalURI));
        }
        return false;
    }
}
