package id.or.ekayana.ekayanaarama.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BaseActivity;
import id.or.ekayana.ekayanaarama.app.file.EventImageDirectory;
import id.or.ekayana.ekayanaarama.constant.ShPrefsName;
import id.or.ekayana.ekayanaarama.data.retrofit.Network;
import id.or.ekayana.ekayanaarama.service.DailyReflectionService;
import id.or.ekayana.ekayanaarama.service.SynchronizeEventListService;

/**
 * Starting activity, showing splash screen.
 *
 * @author Jony
 * @since 01/07/2016
 */
public class MainActivity extends BaseActivity {

    private class MainTask implements Runnable {
        private static final int START_REPEATING_SERVICE_AFTER_MILLIS = 10 * 60000; // 10 minutes

        private final Context mContext;
        private boolean mIsStopped = false;

        public MainTask(Context context) {
            this.mContext = context;
        }

        public void stop() {
            this.mIsStopped = true;
        }

        @Override
        public void run() {
            //initialize netwwork
            Network.initializeDefaultNetwork();

            // Get timestamp of the app's previous run.
            SharedPreferences shPref = PreferenceManager.getDefaultSharedPreferences(mContext);
            long lastTimestamp = shPref.getLong(ShPrefsName.Key.LAST_RUN_TIMESTAMP, 0),
                 currentTimestamp = System.currentTimeMillis();
            long timestampDiff = Math.abs(currentTimestamp - lastTimestamp);

            // Save current timestamp to SharedPreferences.
            shPref.edit().putLong(ShPrefsName.Key.LAST_RUN_TIMESTAMP, currentTimestamp).apply();

            // Create app directories, if not created yet.
            EventImageDirectory.get(mContext).mkdir();

            // Run repeating services.
            if (timestampDiff > START_REPEATING_SERVICE_AFTER_MILLIS) {
                DailyReflectionService.startRepeating(mContext);
                SynchronizeEventListService.startRepeating(mContext, 0);
            }

            // Sleep for a while.
            SystemClock.sleep(1000);

            // Check if the task should be stopped.
            if (mIsStopped) {
                return;
            }

            // Go to home screen.
            Intent intent = new Intent(mContext, HomeActivity.class);
            startActivity(intent);

            // Finish this activity.
            finish();
        }
    }

    private MainTask mMainTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_main);

        // Check if it is the first time the app runs.
        SharedPreferences shPref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isFirstRun = shPref.getBoolean(ShPrefsName.Key.FIRST_RUN, true);
        if (isFirstRun) {
            shPref.edit().putBoolean(ShPrefsName.Key.FIRST_RUN, false).apply();
            // Sync the event list.
            //SynchronizeEventListService.start(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mMainTask = new MainTask(this);
        new Thread(mMainTask).start();
    }

    @Override
    protected void onPause() {
        if (mMainTask != null) {
            mMainTask.stop();
        }
        super.onPause();
    }
}
