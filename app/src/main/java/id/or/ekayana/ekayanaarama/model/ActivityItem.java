package id.or.ekayana.ekayanaarama.model;

import java.util.ArrayList;
import java.util.List;


/**
 * Activity item, contains activity name and its schedules.
 */

public class ActivityItem {

    private long id;
    private final String name;
    private final List<ActivityScheduleItem> listSchedule;
    private final List<String> listInfo;
    private final boolean isInfo;

    public ActivityItem(String name) {
        this.name = name;
        this.listSchedule = new ArrayList<>(1);
        this.listInfo = null;
        this.isInfo = false;
    }

    public ActivityItem(String name, List<ActivityScheduleItem> schedules) {
        this.name = name;
        this.listSchedule = schedules;
        this.listInfo = null;
        this.isInfo = false;
    }

    public ActivityItem(List<String> listInfo) {
        this.name = null;
        this.listSchedule = null;
        this.listInfo = listInfo;
        this.isInfo = true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public List<ActivityScheduleItem> getScheduleList() {
        return listSchedule;
    }

    public List<String> getInfoList() {
        return listInfo;
    }

    public boolean isInfo() {
        return isInfo;
    }

    public void addSchedule(ActivityScheduleItem scheduleItem) {
        listSchedule.add(scheduleItem);
    }

}
