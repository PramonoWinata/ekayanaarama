package id.or.ekayana.ekayanaarama.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Utils for String.
 *
 * @author Jony
 * @since 02/07/2016
 */
public class StringUtils {
    private static final String URL_ENCODING = "UTF-8";

    /**
     * Checks if a string is <code>null</code> or empty.
     *
     * @param s
     * @return
     */
    public static boolean isNullOrEmpty(String s) {
        return (s == null || s.isEmpty());
    }

    /**
     * Checks if two strings are equal (case-sensitive).
     *
     * @param s1
     * @param s2
     * @return
     */
    public static boolean equals(String s1, String s2) {
        if (s1 == null || s2 == null) {
            return false;
        }
        return s1.equals(s2);
    }

    /**
     * Checks if two strings are equal.
     *
     * @param s1
     * @param s2
     * @param ignoreCase
     * @return
     */
    public static boolean equals(String s1, String s2, boolean ignoreCase) {
        if (s1 == null || s2 == null) {
            return false;
        }
        return (ignoreCase)
                ? s1.equalsIgnoreCase(s2)
                : s1.equals(s2);
    }

    /**
     * Equals calling <code>URLEncoder.encode(s, enc)</code>,
     * and then the "+" signs (the result of encoding space characters) will be converted to <code>"%20"</code>.
     *
     * @param s The string to be encoded
     * @param enc The name of the supported character encoding
     * @return The encoded string
     * @throws UnsupportedEncodingException If the named encoding is not supported
     */
    public static String encodeURIComponent(String s, String enc) throws UnsupportedEncodingException {
        return URLEncoder.encode(s, enc).replace("+", "%20");
    }

    /**
     * Equals calling <code>URLEncoder.encode(s, "UTF-8")</code>,
     * and then the "+" signs (the result of encoding space characters) will be converted to <code>"%20"</code>.
     *
     * @param s The string to be encoded
     * @return The encoded string
     * @throws UnsupportedEncodingException If the named encoding is not supported
     */
    public static String encodeURIComponent(String s) throws UnsupportedEncodingException {
        return encodeURIComponent(s, URL_ENCODING);
    }

    /**
     * Equals calling <code>URLDecoder.decode(s, enc)</code>
     * after converting characters "%20" to "+" signs.
     *
     * @param s The string to be decoded
     * @param enc The name of the supported character encoding
     * @return The decoded string
     * @throws UnsupportedEncodingException If the named encoding is not supported
     */
    public static String decodeURIComponent(String s, String enc) throws UnsupportedEncodingException {
        return URLDecoder.decode(s.replace("%20", "+"), enc);
    }

    /**
     * Equals calling <code>URLDecoder.decode(s, "UTF-8")</code>
     * after converting characters "%20" to "+" signs.
     *
     * @param s The string to be decoded
     * @return The decoded string
     * @throws UnsupportedEncodingException If the named encoding is not supported
     */
    public static String decodeURIComponent(String s) throws UnsupportedEncodingException {
        return decodeURIComponent(s, URL_ENCODING);
    }
}
