package id.or.ekayana.ekayanaarama.data.retrofit;

import android.os.Build;
import android.util.Log;

import com.google.android.gms.security.ProviderInstaller;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import id.or.ekayana.ekayanaarama.activity.MainActivity;
import id.or.ekayana.ekayanaarama.app.AppConfig;
import id.or.ekayana.ekayanaarama.app.MainApplication;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Network {
    private static EkayanaEndpoint endpoint;
    private static OkHttpClient okHttpClient;

    public static EkayanaEndpoint endpoint(){
        return Network.endpoint;
    }

    public static  OkHttpClient okHttpClient(){
        return okHttpClient;
    }

    public static void initializeDefaultNetwork(){
        if(endpoint==null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            client.readTimeout(120, TimeUnit.SECONDS);
            client.connectTimeout(60, TimeUnit.SECONDS);

            //if(!Constants.ENVIRONTMENT.equals("production")){
            //    client.addInterceptor(logging);
            //}

            enableTls12OnPreLollipop(client);

            //client.addInterceptor(new UpdateTokenHeaderInterceptor());

            Gson excludeGson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {

                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    //ExcludeOnJson annotation = f.getAnnotation(ExcludeOnJson.class);
                    //return (annotation!=null);

                    return false;
                }

                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }
            }).create();

            okHttpClient = client.build();

            Retrofit retrofit = new Retrofit.Builder().client(okHttpClient)
                    .baseUrl(AppConfig.SERVER_HOST)
                    .addConverterFactory(GsonConverterFactory.create(excludeGson))
                    .build();

            Network.endpoint = retrofit.create(EkayanaEndpoint.class);
        }
    }

    private static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder client) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
            try {
                //ProviderInstaller.installIfNeeded(MainApplication.getContext());
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);

                client.sslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()), defaultTrustManager());

                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build();

                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);

                client.connectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc);
            }
        }

        return client;
    }

    /** Returns a trust manager that trusts the VM's default certificate authorities. */
    private static X509TrustManager defaultTrustManager() throws GeneralSecurityException {
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init((KeyStore) null);
        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
            throw new IllegalStateException("Unexpected default trust managers:"
                    + Arrays.toString(trustManagers));
        }
        return (X509TrustManager) trustManagers[0];
    }
}