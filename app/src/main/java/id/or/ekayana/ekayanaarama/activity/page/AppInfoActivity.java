package id.or.ekayana.ekayanaarama.activity.page;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;
import id.or.ekayana.ekayanaarama.constant.Strings;

/**
 *
 * @author Jony
 * @since 02/07/2016
 */
public class AppInfoActivity extends BasePageActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_app_info);
        TextView textView = (TextView) findViewById(R.id.app_info);

        // Get application name.
        String appName = getString(getApplicationInfo().labelRes);

        // Get current app version.
        String appVersion;
        try {
            PackageInfo pkgInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pkgInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            appVersion = Strings.EMPTY;
        }

        // Generate application info.
        /* String sInfo = appName + "\nVer. " + appVersion
                + "\n\nTeam Developer:\nJony, Iman Tunggono, Harris"
                + "\n\nRenungan Harian:\n“25 Tahun Karaniya”\n© Penerbit Karaniya"
                + "\n\nBuddhavacana:\n“Tutur Bijak”\n© Penerbit Karaniya"
                + "\n\nSaran & Kritik:\nadmin@ekayana.or.id";
        textView.setText(sInfo); */
        String sInfo = "<strong>" + appName + "</strong><br />Ver. " + appVersion
                + "<br /><br /><strong>Team Developer</strong><br />Jony, Iman Tunggono, Harris"
                + "<br /><br /><strong>Renungan Harian</strong><br />“25 Tahun Karaniya”<br />&copy; Penerbit Karaniya"
                + "<br /><br /><strong>Buddhavacana</strong><br />“Tutur Bijak”<br />&copy; Penerbit Karaniya"
                + "<br /><br /><strong>Saran &amp; Kritik</strong><br /><a href=\"mailto:admin@ekayana.or.id\">admin@ekayana.or.id</a>";
        textView.setText(Html.fromHtml(sInfo));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
