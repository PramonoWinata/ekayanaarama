package id.or.ekayana.ekayanaarama.receiver;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import java.text.SimpleDateFormat;
import java.util.Date;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;
import id.or.ekayana.ekayanaarama.activity.page.EventsActivity;

/**
 *
 * @author Jony
 * @since 14/02/2017
 */
public class NotificationPublisher extends BroadcastReceiver {

    /*
    public static void startRepeating(Context context, int count) {
        Intent intent = new Intent(context, NotificationPublisher.class);
        intent.putExtra("nid", Integer.parseInt(String.valueOf(System.currentTimeMillis()).substring(7)));
        intent.putExtra("count", count);
        intent.putExtra("time", new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        // alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 60000, alarmIntent);
        alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 5000, 30000, alarmIntent);
    }
    */

    @Override
    public void onReceive(Context context, Intent intent) {
        /*
        // int nid = intent.getIntExtra("nid", 2);
        int nid = Integer.parseInt(String.valueOf(System.currentTimeMillis()).substring(7));
        int count = intent.getIntExtra("count", 0);
        // String time = intent.getStringExtra("time");
        String time = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date());

        Intent notifIntent = new Intent(context, EventsActivity.class);
        PendingIntent pendingNotifIntent = PendingIntent.getActivity(context, 0, notifIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logo_small)
                .setContentTitle("Scheduled Notif #" + count)
                .setContentText("Time: " + time)
                .setContentIntent(pendingNotifIntent)
                .setAutoCancel(true);

        NotificationManager notifMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notifMgr.notify(nid, notifBuilder.build());

        // startRepeating(context, count + 1);
        */
    }

    public static void publish(Context context, String title, String message) {
        int nid = Integer.parseInt(String.valueOf(System.currentTimeMillis()).substring(7));
        publish(context, nid, title, message, null);
    }

    public static void publish(Context context, String title, String message, Class<? extends BasePageActivity> activityClass) {
        int nid = Integer.parseInt(String.valueOf(System.currentTimeMillis()).substring(7));
        publish(context, nid, title, message, activityClass);
    }

    public static void publish(Context context, int notificationID, String title, String message, Class<? extends BasePageActivity> activityClass) {
        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logo_small)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true);

        if (activityClass != null) {
            Intent notifIntent = new Intent(context, activityClass);
            PendingIntent pendingNotifIntent = PendingIntent.getActivity(context, 0, notifIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            notifBuilder.setContentIntent(pendingNotifIntent);
        }

        NotificationManager notifMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notifMgr.notify(notificationID, notifBuilder.build());
    }
}
