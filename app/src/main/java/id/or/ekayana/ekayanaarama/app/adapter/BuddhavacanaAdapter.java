package id.or.ekayana.ekayanaarama.app.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.page.BuddhavacanaActivity.BuddhavacanaArticle;

/**
 * Adapter for Buddhavacana.
 *
 * @author Jony
 * @since 07/08/2016
 */
public class BuddhavacanaAdapter extends ArrayAdapter<BuddhavacanaArticle> {
    private final List<BuddhavacanaArticle> mListItem;

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param resource The resource ID for a layout file containing a TextView to use when
     *                 instantiating views.
     * @param listItem The list of items to represent in the ListView.
     */
    public BuddhavacanaAdapter(Context context, int resource, List<BuddhavacanaArticle> listItem) {
        super(context, resource, listItem);
        mListItem = listItem;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the  view.
        View view = super.getView(position, convertView, parent);
        // Get the item of the specified position.
        BuddhavacanaArticle item = getItem(position);
        // Set the item to the view's tags.
        view.setTag(item);
        // Return the view.
        return view;
    }
}
