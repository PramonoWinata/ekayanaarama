package id.or.ekayana.ekayanaarama.activity.base;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.app.MainApplication;

/**
 * Base class to be extended by activities other than
 * {@link id.or.ekayana.ekayanaarama.activity.MainActivity} and
 * {@link id.or.ekayana.ekayanaarama.activity.HomeActivity}.
 *
 * @author Jony
 * @since 02/07/2016
 */
public abstract class BasePageActivity extends AppCompatActivity {

    public MainApplication getApplicationCasted() {
        return (MainApplication) getApplication();
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
        overridePendingTransition(R.anim.slide_in, android.R.anim.fade_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_out);
    }
}
