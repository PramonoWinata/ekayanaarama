package id.or.ekayana.ekayanaarama.activity.base;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import id.or.ekayana.ekayanaarama.app.MainApplication;

/**
 * Base class to be extended by all activities.
 *
 * @author Jony
 * @since 02/07/2016
 */
public abstract class BaseActivity extends AppCompatActivity {

    public MainApplication getApplicationCasted() {
        return (MainApplication) getApplication();
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /* public int convertDpToPixel(float dp) {
        float density = getResources().getDisplayMetrics().density;
        return (int) Math.ceil(density * dp);
    } */
}
