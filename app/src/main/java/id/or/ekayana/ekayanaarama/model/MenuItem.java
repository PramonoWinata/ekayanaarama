package id.or.ekayana.ekayanaarama.model;

import android.app.Activity;

import id.or.ekayana.ekayanaarama.constant.Strings;

/**
 * Menu item.
 *
 * @author Jony
 * @since 02/07/2016.
 */
public class MenuItem {
    private final int id;
    private final String name;
    private final int iconResID;
    private final Class<? extends Activity> activityClass;
    private boolean isEnabled;

    /**
     * Constructor for empty item. The menu item is disabled by default.
     */
    public MenuItem() {
        this.id = -1;
        this.name = Strings.EMPTY;
        this.iconResID = -1;
        this.activityClass = null;
        this.isEnabled = false;
    }

    /**
     * Constructor.
     *
     * @param id The ID of the menu item
     * @param name The name/title of the menu item
     * @param activity The class of {@code Activity} to be started when the menu item is selected
     */
    public MenuItem(int id, String name, Class<? extends Activity> activity) {
        this.id = id;
        this.name = name;
        this.iconResID = -1;
        this.activityClass = activity;
        this.isEnabled = true;
    }

    /**
     * Constructor.
     *
     * @param id The ID of the menu item
     * @param name The name/title of the menu item
     * @param iconResID The resource ID for the menu item's icon
     * @param activity The class of {@code Activity} to be started when the menu item is selected
     */
    public MenuItem(int id, String name, int iconResID, Class<? extends Activity> activity) {
        this.id = id;
        this.name = name;
        this.iconResID = iconResID;
        this.activityClass = activity;
        this.isEnabled = true;
    }

    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean hasIcon() {
        return iconResID > 0;
    }

    public int getIconResourceID() {
        return iconResID;
    }

    public Class<? extends Activity> getActivityClass() {
        return activityClass;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public boolean equals(MenuItem m) {
        return (m != null && m.id == this.id);
    }

    public MenuItem disable() {
        this.isEnabled = false;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o.getClass().equals(this.getClass())) {
            MenuItem other = (MenuItem) o;
            return (other.id == this.id);
        }
        return false;
    }

    @Override
    public String toString() {
        return name;
        /* return super.toString() + ": { "
                + "id: " + id + ", "
                + "name: " + name + " "
            + "} "; */
    }
}