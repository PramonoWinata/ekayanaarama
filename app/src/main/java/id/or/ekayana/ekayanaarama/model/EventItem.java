package id.or.ekayana.ekayanaarama.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.or.ekayana.ekayanaarama.app.AppConfig;

/**
 * Event item.
 *
 * @author Jony
 * @since 02/12/2016
 */

public class EventItem{

    @SerializedName("id")
    private long id;
    @SerializedName("isActiveRecord")
    private boolean isActiveRecord;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("publishDate")
    private String  publishDate;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("endDate")
    private String endDate;
    @SerializedName("timestamp")
    private long lastModified;

    private List<EventImageItem> listImage;
    private String thumbnailURI;

    @SerializedName("imageURIs")
    private List<String> ImageUris;

    public EventItem(long id) {
        this.id = id;
        this.isActiveRecord = true;
    }

    public EventItem(long id, boolean isActiveRecord) {
        this.id = id;
        this.isActiveRecord = isActiveRecord;
    }

    public long getID() {
        return id;
    }

    public boolean isActiveRecord() {
        return isActiveRecord;
    }

    public String getTitle() {
        return title;
    }

    public boolean hasDescription() {
        return (description != null && !description.isEmpty());
    }

    public String getDescription() {
        return description;
    }

    public String getPublishDateSQL() {
        return publishDate;
    }

    public String getStartDateSQL() {
        return startDate;
    }

    public String getEndDateSQL() {
        return endDate;
    }

    public List<EventImageItem> getImageList() {
        return listImage;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setActiveRecord(boolean activeRecord) {
        isActiveRecord = activeRecord;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPublishDateSQL(String publishDate) {
        this.publishDate = publishDate;
    }

    public void setStartDateSQL(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDateSQL(String endDate) {
        this.endDate = endDate;
    }

    public void setImages(List<EventImageItem> listImage) {
        this.listImage = listImage;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public boolean equals(EventItem o) {
        return (o != null && o.id == this.id);
    }

    public String getThumbnailURI() {
        return thumbnailURI;
    }

    public void setThumbnailURI(String thumbnailURI) {
        this.thumbnailURI = thumbnailURI;
    }

    public void setThumbnailURIFirstTime(String thumbnailURI) {
        this.thumbnailURI = AppConfig.SERVER_HOST + thumbnailURI;
    }

    public List<String> getImageUris() {
        return ImageUris;
    }

    public void setImageUris(List<String> imageUris) {
        ImageUris = imageUris;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o.getClass().equals(this.getClass())) {
            EventItem other = (EventItem) o;
            return (other.id == this.id);
        }
        return false;
    }

    @Override
    public String toString() {
        return title;
    }
}
