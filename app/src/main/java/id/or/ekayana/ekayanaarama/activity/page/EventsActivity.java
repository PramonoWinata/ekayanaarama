package id.or.ekayana.ekayanaarama.activity.page;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.activity.base.BasePageActivity;
import id.or.ekayana.ekayanaarama.app.adapter.EventAdapter;
import id.or.ekayana.ekayanaarama.data.db.EventDA;
import id.or.ekayana.ekayanaarama.data.retrofit.Network;
import id.or.ekayana.ekayanaarama.model.EventItem;
import id.or.ekayana.ekayanaarama.model.EventItemList;
import id.or.ekayana.ekayanaarama.service.SynchronizeEventListService;
import retrofit2.Call;
import retrofit2.Response;

/**
 *
 * @author Jony
 * @since 02/07/2016
 */
public class EventsActivity extends BasePageActivity {

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Check intent action.
            if (SynchronizeEventListService.ACTION_REPORT_STATUS.equals(intent.getAction())) {
                //onEventListSynced(intent);
            }

            // Unregister the local broadcast receiver.
            // unregisterLocalReceiver();
        }
    };

    private boolean mLocalReceiverRegistered = false;

    private void registerLocalReceiver() {
        if (!mLocalReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter(SynchronizeEventListService.ACTION_REPORT_STATUS);
            LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, intentFilter);
            mLocalReceiverRegistered = true;
            System.out.println("Registered local broadcast receiver.");
        }
    }

    private void unregisterLocalReceiver() {
        if (mLocalReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
            mLocalReceiverRegistered = false;
            System.out.println("Unregistered local broadcast receiver.");
        }
    }

    private void onEventListSynced(Intent intent) {
        boolean success = intent.getBooleanExtra(SynchronizeEventListService.EXTRA_SUCCESS, false);
        if (success) {
            boolean hasUpdate = intent.getBooleanExtra(SynchronizeEventListService.EXTRA_HAS_UPDATES, false);
            if (hasUpdate) {
                loadEventList();
                Toast.makeText(this, "Event updated successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "No event updated", Toast.LENGTH_SHORT).show();
            }
            mRefreshLayout.setRefreshing(false);
        } else {
            String message = intent.getStringExtra(SynchronizeEventListService.EXTRA_MESSAGE);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    private SwipeRefreshLayout mRefreshLayout = null;
    private TextView mTextMessage = null;
    private ListView mListView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view.
        setContentView(R.layout.activity_events);

        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mTextMessage = (TextView) findViewById(R.id.message);
        mListView = (ListView) findViewById(R.id.list);

        // Set the color scheme for the refresh progress indicator.
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        // Set the pull-to-refresh listener.
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //SynchronizeEventListService.start(EventsActivity.this);
                loadEventList();
            }
        });

        // Set the item click listener.
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected event item.
                EventItem event = (EventItem) view.getTag();

                // Start the detail activity with the selected item.
                EventDetailActivity.startActivity(EventsActivity.this, event);
            }
        });

        // Set enabled/disabled pull-to-refresh based on list view scrolling position.
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                mRefreshLayout.setEnabled(view.getFirstVisiblePosition() == 0);
            }
        });

        // Load the event list.
        loadEventList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerLocalReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterLocalReceiver();
    }

    private void loadEventList() {
        new LoadEventTask(this).execute();
    }

    private class LoadEventTask extends AsyncTask<Context, Integer, List<EventItem>> {

        private final Context mContext;
        private EventDA eventDA;

        public LoadEventTask(Context context) {
            this.mContext = context;
            this.eventDA = new EventDA(context);
        }

        @Override
        protected List<EventItem> doInBackground(Context... params) {

        List<EventItem> itemList = new ArrayList<>();

        //Getting event list by retrofit
        try{
            Call<EventItemList> call = Network.endpoint().getEventList(1231231987);
            Response<EventItemList> response = call.execute();

            if(response.isSuccessful())
            {
                EventItemList eventItemList = response.body();
                itemList = eventItemList.getListEvent();

                for(int i =0;i<itemList.size();i++)
                {

                    while(itemList.get(i).getTitle() == null) {
                        itemList.remove(i);
                    }

                    if(itemList.get(i).getImageUris().size() > 0)
                        itemList.get(i).setThumbnailURIFirstTime(itemList.get(i).getImageUris().get(0));

                }

                //save it to db
                int row = eventDA.updateEvents(itemList);

            }
            else
            {
                Log.d("eventList", "ERROR CODE:  " + response.code());
            }
        }

        catch (Exception e){
            Log.d("eventList", "ERROR MESSAGE :" + e.getMessage());
        }

            //get event list from database
            List<EventItem> eventList = new EventDA(mContext).getEventList();
            return eventList;
        }

        @Override
        protected void onPostExecute(List<EventItem> listEvent) {
            // TextView mTextMessage = (TextView) findViewById(R.id.message);
            // ListView mListView = (ListView) findViewById(R.id.list);

            mRefreshLayout.setRefreshing(false);

            if (listEvent == null || listEvent.isEmpty()) {
                mTextMessage.setVisibility(View.VISIBLE);
                mListView.setVisibility(View.GONE);
                mListView.setAdapter(null);
            } else {
                mTextMessage.setVisibility(View.GONE);
                mListView.setVisibility(View.VISIBLE);
                EventAdapter adapter = new EventAdapter(mContext, listEvent);
                mListView.setAdapter(adapter);
            }
        }
    }
}
