package id.or.ekayana.ekayanaarama.app.exception;

/**
 * Thrown when attempting to send a request when the network is not connected.
 *
 * @author Jony
 * @since 13/11/2016
 */

public class NetworkNotConnectedException extends RuntimeException {
    public NetworkNotConnectedException() {
    }

    public NetworkNotConnectedException(String detailMessage) {
        super(detailMessage);
    }

    public NetworkNotConnectedException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public NetworkNotConnectedException(Throwable throwable) {
        super(throwable);
    }
}
