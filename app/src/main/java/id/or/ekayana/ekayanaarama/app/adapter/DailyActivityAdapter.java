package id.or.ekayana.ekayanaarama.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.model.ActivityItem;
import id.or.ekayana.ekayanaarama.model.EventItem;
import id.or.ekayana.ekayanaarama.util.DateUtils;

public class DailyActivityAdapter extends RecyclerView.Adapter<DailyActivityAdapter.MyViewHolder> {

    private List<ActivityItem> activityList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
        }
    }

    public DailyActivityAdapter(List<ActivityItem> activityList) {
        this.activityList = activityList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_daily_activities, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ActivityItem movie = activityList.get(position);
        holder.title.setText(movie.getName());
    }


    @Override
    public int getItemCount() {
        return activityList.size();
    }

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param listItem The list of event items to represent.
     */



/*    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // Get item of the specified position.
        ActivityItem event = getItem(position);

        // Generate the view.
        if (view == null) {
            view = inflate(R.layout.list_daily_activities, null);
        }

        TextView txtTitle = (TextView) view.findViewById(R.id.title);
        TextView txtDesc = (TextView) view.findViewById(R.id.desc);

        txtTitle.setText(event.getName());

        // Return the view.
        return view;
    }*/
}
