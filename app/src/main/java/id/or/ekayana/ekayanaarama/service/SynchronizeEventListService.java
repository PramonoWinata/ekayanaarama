package id.or.ekayana.ekayanaarama.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import id.or.ekayana.ekayanaarama.activity.page.EventsActivity;
import id.or.ekayana.ekayanaarama.app.exception.NetworkNotConnectedException;
import id.or.ekayana.ekayanaarama.app.file.EventImageDirectory;
import id.or.ekayana.ekayanaarama.constant.NotificationID;
import id.or.ekayana.ekayanaarama.data.api.GetUpdatedEventListAPI;
import id.or.ekayana.ekayanaarama.data.db.EventDA;
import id.or.ekayana.ekayanaarama.model.EventImageItem;
import id.or.ekayana.ekayanaarama.model.EventItem;
import id.or.ekayana.ekayanaarama.receiver.NotificationPublisher;
import id.or.ekayana.ekayanaarama.util.ListUtils;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests
 * to synchronize the event list saved in local data with the server.
 */
public class SynchronizeEventListService extends IntentService {

    /**
     * Local SharedPreferences to be used only by this service.
     */
    private static class LocalSharedPreferences {
        private static final String FILE_NAME = SynchronizeEventListService.class.getCanonicalName() + "_preferences";
        private static final String KEY_LAST_SYNC_TIMESTAMP = "lastSyncTimestamp";

        private static LocalSharedPreferences getInstance(Context context) {
            return new LocalSharedPreferences(context);
        }

        private final Context mContext;

        private LocalSharedPreferences(Context context) {
            this.mContext = context;
        }

        public SharedPreferences getSharedPreferences(Context context) {
            return context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        }

        public long getLastSyncTimestamp() {
            return getSharedPreferences(mContext).getLong(KEY_LAST_SYNC_TIMESTAMP, 0);
        }

        public void setLastSyncTimestamp(long timestamp) {
            getSharedPreferences(mContext).edit()
                    .putLong(KEY_LAST_SYNC_TIMESTAMP, timestamp)
                    .apply();
        }
    }

    /* Sync Schedule Configurations */
    private static final int ALARM_INTERVAL = 45 * 60 * 1000,   // 45 minutes
                             SYNC_INTERVAL  = 3 * 3600 * 1000;  // 3 hours
    /*
    private static final int ALARM_INTERVAL = 30 * 1000, // TEST
                             SYNC_INTERVAL  = 30 * 1000; // TEST
                             */

    private static String CANONICAL_NAME = SynchronizeEventListService.class.getCanonicalName();

    private static final String ACTION_SYNCHRONIZE = "id.or.ekayana.ekayanaarama.service.action.SYNCHRONIZE",
                             // ACTION_SYNCHRONIZE_DELAYED = "id.or.ekayana.ekayanaarama.service.action.SYNCHRONIZE_DELAYED",
                                ACTION_SYNCHRONIZE_REPEATING = "id.or.ekayana.ekayanaarama.service.action.SYNCHRONIZE_REPEATING";

    public static final String EXTRA_PUBLISH_UPDATE_TO_NOTIFICATION = CANONICAL_NAME + ".extra.PUBLISH_UPDATE_TO_NOTIFICATION";

    public static final String ACTION_REPORT_STATUS = CANONICAL_NAME + ".action.REPORT_STATUS";
    public static final String EXTRA_SUCCESS = CANONICAL_NAME + ".extra.SUCCESS",
                               EXTRA_HAS_UPDATES = CANONICAL_NAME + ".extra.UPDATED",
                               EXTRA_MESSAGE = CANONICAL_NAME + ".extra.MESSAGE";

    /**
     * Starts the service.
     * @param context
     */
    public static void start(Context context) {
        System.out.println("Starting service " + SynchronizeEventListService.class.getSimpleName() + "...");
        Intent intent = new Intent(context, SynchronizeEventListService.class);
        intent.setAction(ACTION_SYNCHRONIZE);
        context.startService(intent);
    }

    /**
     * Starts the service if specified <code>checkIntervalMillis</code> has passed since previous run timestamp.
     * @param context
     * @param checkIntervalMillis
     * @return If the service was started
     *
    public static boolean start(Context context, long checkIntervalMillis) {
        long sinceLastSync = System.currentTimeMillis() - LocalSharedPreferences.getInstance(context).getLastSyncTimestamp();
        if (sinceLastSync >= checkIntervalMillis || sinceLastSync < 0) {
            start(context);
            return true;
        }
        return false;
    } */

    /**
     * Starts the service after the specified delay.
     * @param context
     * @param delayMillis
     * @return If the service was started
     *
    public static void startDelayed(Context context, int delayMillis) {
        Intent intent = new Intent(context, SynchronizeEventListService.class);
        intent.setAction(ACTION_SYNCHRONIZE_DELAYED);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + delayMillis, pendingIntent);
    } */

    /**
     * Starts the service in a repeating interval.
     * @param context
     * @param firstRunMillis
     */
    public static void startRepeating(Context context, int firstRunMillis) {
        // Calculate first run trigger interval.
        long sinceLastSync = System.currentTimeMillis() - LocalSharedPreferences.getInstance(context).getLastSyncTimestamp();
        long firstRunInterval = (sinceLastSync >= ALARM_INTERVAL || sinceLastSync < 0)
                ? firstRunMillis
                : ALARM_INTERVAL - sinceLastSync;
        if (firstRunInterval < firstRunMillis) {
            firstRunInterval = firstRunMillis;
        }

        // Set the repeating alarm.
        Intent intent = new Intent(context, SynchronizeEventListService.class);
        intent.setAction(ACTION_SYNCHRONIZE_REPEATING);
        intent.putExtra(EXTRA_PUBLISH_UPDATE_TO_NOTIFICATION, true);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        PendingIntent alarmIntent = PendingIntent.getService(context, 0, intent, 0);
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + firstRunInterval, ALARM_INTERVAL, alarmIntent);
    }

    public SynchronizeEventListService() {
        super("SynchronizeEventListService");
    }

    /**
     * Handles requests to the service.
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String intentAction = intent.getAction();

            System.out.println("Service " + getClass().getSimpleName() + " started (action=" + intentAction + ").");
            if (intentAction == null || intentAction.isEmpty()) {
                return;
            }

            switch (intentAction) {
                case ACTION_SYNCHRONIZE:
                    //long ms = System.currentTimeMillis();
                    //String startedTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
                    handleActionSynchronize(false);
                    //ms = System.currentTimeMillis() - ms;
                    //System.out.println("Started: " + startedTime + ", Elapsed: " + ms + " ms");
                    break;

                /* case ACTION_SYNCHRONIZE_DELAYED:
                    handleActionSynchronize();
                    break; */

                case ACTION_SYNCHRONIZE_REPEATING:
                    // Skip operation between 00:00:00 to 05:59:59.
                    int hh = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                    if (hh >= 6) {
                        // Check last sync timestamp, skip if less than allowed sync interval.
                        long sinceLastSync = System.currentTimeMillis() - LocalSharedPreferences.getInstance(this).getLastSyncTimestamp();
                        if (sinceLastSync >= SYNC_INTERVAL || sinceLastSync < 0) {
                            handleActionSynchronize(true);
                        }
                    }
                    break;
            }
        }
    }

    /**
     * Handles synchronize action.
     */
    private void handleActionSynchronize(boolean publishUpdatesToNotification) {
        try {
            // Get current timestamp.
            long timestamp = System.currentTimeMillis();

            // Get timestamp of last modified records.
            EventDA eventDA = new EventDA(this);
            long lastTimestamp = eventDA.getLatestRecordTimestamp();

            // Get updated event list from the API.
            GetUpdatedEventListAPI.Response result = new GetUpdatedEventListAPI(this).execute(lastTimestamp);
            if (result.hasUpdatedEvents()) {
                // Get the list of updated events and save to database.
                List<EventItem> listEvent = result.getEventList();
                int rowCount = eventDA.updateEvents(listEvent);
                if (rowCount == 0) {
                    // No updated events.
                    sendSuccess(false);
                } else {
                    // Publish the updates to notification, if necessary.
                    if (publishUpdatesToNotification) {
                        NotificationPublisher.publish(this, NotificationID.EVENT_UPDATE, "Event Update", "Cek event terbaru di Ekayana.", EventsActivity.class);
                    }

                    // Main task success.
                    sendSuccess(true);
                }

                // Delete unused saved event image files.
                try {
                    File[] imageFiles = EventImageDirectory.get(this).listFiles();
                    if (imageFiles != null && imageFiles.length > 0) {
                        List<String> listSavedFileName = eventDA.getSavedEventImageFileList();
                        if (listSavedFileName.isEmpty()) {
                            // No saved file name in database.
                            // Delete all the files.
                            for (File file : imageFiles) {
                                file.delete();
                            }
                        } else {
                            // Check existing files whose file name is not saved in database.
                            for (File file : imageFiles) {
                                String fileName = file.getName();
                                boolean found = false;
                                for (int i = 0, size = listSavedFileName.size(); i < size; i++) {
                                    if (listSavedFileName.get(i).equals(fileName)) {
                                        found = true; // The file name is saved in database.
                                        listSavedFileName.remove(i); // Remove to prevent double-checking.
                                        break;
                                    }
                                }
                                if (!found) {
                                    file.delete(); // Delete the file.
                                }
                            }
                        }
                    }
                } catch (Exception ex) { }

                // Download new/updated images.
                try {
                    List<EventImageItem> listUnsavedEventImage = eventDA.getUnsavedEventImageList();
                    if (!listUnsavedEventImage.isEmpty()) {
                        // Group by event ID.
                        long lastEventID = -1;
                        List<Integer> listOrderNumber = new ArrayList<>(2);
                        for (EventImageItem item : listUnsavedEventImage) {
                            long eventID = item.getEventID();
                            if (eventID != lastEventID) {
                                if (lastEventID != -1) {
                                    DownloadEventImageService.start(this, lastEventID, ListUtils.toArray(listOrderNumber));
                                    listOrderNumber.clear();
                                }
                                lastEventID = eventID;
                            }
                            listOrderNumber.add(item.getOrderNumber());
                        }
                        DownloadEventImageService.start(this, lastEventID, ListUtils.toArray(listOrderNumber));
                    }
                } catch (Exception ex) { }
            } else {
                // No updated events.
                sendSuccess(false);
            }

            // Save the successful sync timestamp.
            LocalSharedPreferences.getInstance(this).setLastSyncTimestamp(timestamp);
        } catch (NetworkNotConnectedException ex) {
            sendError("Not connected to network");
        } catch (SocketException ex) {
            sendError("Connection to server failed");
        } catch (IOException ex) {
            sendError("Connection to server failed");
        } catch (RuntimeException ex) {
            sendError("An error occurred");
        }
    }

    /**
     * Send result of a running action as successful.
     * @param hasUpdate If the event list has updates
     */
    private void sendSuccess(boolean hasUpdate) {
        Intent intent = new Intent(ACTION_REPORT_STATUS)
                .putExtra(EXTRA_SUCCESS, true)
                .putExtra(EXTRA_HAS_UPDATES, hasUpdate);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Send result of a running action as failed.
     * @param message The message to be displayed
     */
    private void sendError(String message) {
        Intent intent = new Intent(ACTION_REPORT_STATUS)
                .putExtra(EXTRA_SUCCESS, false)
                .putExtra(EXTRA_MESSAGE, message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
