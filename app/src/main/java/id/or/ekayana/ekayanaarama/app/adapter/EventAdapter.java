package id.or.ekayana.ekayanaarama.app.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

import id.or.ekayana.ekayanaarama.R;
import id.or.ekayana.ekayanaarama.model.EventItem;
import id.or.ekayana.ekayanaarama.util.DateUtils;

/**
 * @author Jony
 * @since 26/12/2016
 */

public class EventAdapter extends CustomAdapter<EventItem> {
    private final SimpleDateFormat mDateFormatter;

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param listItem The list of event items to represent.
     */
    public EventAdapter(Context context, List<EventItem> listItem) {
        super(context, listItem);
        this.mDateFormatter = new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getID();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // Get item of the specified position.
        EventItem event = getItem(position);

        // Generate the view.
        if (view == null) {
            view = inflate(R.layout.list_event, null);
        }

        ImageView imgView = (ImageView) view.findViewById(R.id.image);
        Picasso.with(getContext()).load(event.getThumbnailURI()).fit().centerCrop().into(imgView);

        TextView txtTitle = (TextView) view.findViewById(R.id.title);
        TextView txtDate  = (TextView) view.findViewById(R.id.date);

        txtTitle.setText(event.getTitle());
        String sqlStartDate = event.getStartDateSQL(),
               sqlEndDate   = event.getEndDateSQL();
        if (sqlStartDate.equals(sqlEndDate)) {
            txtDate.setText(mDateFormatter.format(DateUtils.parseSqlDate(sqlStartDate)));
        } else {
            String sDate = mDateFormatter.format(DateUtils.parseSqlDate(sqlStartDate))
                    + " - " + mDateFormatter.format(DateUtils.parseSqlDate(sqlEndDate));
            txtDate.setText(sDate);
        }

        // Set the item to the view's tags.
        view.setTag(event);

        // Return the view.
        return view;
    }
}
