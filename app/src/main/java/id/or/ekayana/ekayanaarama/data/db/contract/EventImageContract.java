package id.or.ekayana.ekayanaarama.data.db.contract;

import android.provider.BaseColumns;

/**
 * @author Jony
 * @since 04/12/2016
 */

public interface EventImageContract {
    public static final String TABLE_NAME = "event_images";
    public static interface Columns extends BaseColumns {
        public static final String
                EVENT_ID        = "event_id",
                ORDER_NUMBER    = "order_number",
                ORIGINAL_URI    = "original_uri",
                SAVED_AS        = "saved_as",
                SAVED_TIME      = "saved_time";
    }
}
