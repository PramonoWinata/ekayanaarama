package id.or.ekayana.ekayanaarama.app.file;

import android.content.Context;

import java.io.File;

/**
 * {@code File} which represents an event image file.
 *
 * @author Jony
 * @since 28/12/2016
 */

public class EventImageFile extends File {
    public EventImageFile(Context context, String fileName) {
        super(EventImageDirectory.get(context), fileName);
    }
}
